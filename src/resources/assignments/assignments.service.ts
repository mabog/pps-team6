import { Injectable } from '@nestjs/common';
import {
  RequirementsDataService
} from '../requirements/requirements-data.service';
import { AssignmentsDataService } from './assignments-data.service';
import { plainToClass } from 'class-transformer';
import {
  AssignmentReturnDTO
} from '../../models/assignments/assignment-return.dto';
import { Assignment } from '../../database/entities/assignment.entity';
import {
  RequirementUpdateDTO
} from '../../models/requirements/requirement-update.dto';
import { UserDisplayDTO } from '../../models/users/user-display.dto';
import {
  AssignmentCreateDTO
} from '../../models/assignments/assignment-create.dto';
import {
  AssignmentUpdateDTO
} from '../../models/assignments/assignment-update.dto';

@Injectable()
export class AssignmentsService {
  public constructor(
    private readonly assignmentsDataService: AssignmentsDataService,
    private readonly requirementsDataService: RequirementsDataService,
  ) { }

  public async getAllEmployeeAssignments(
    employeeId: string,
  ): Promise<AssignmentReturnDTO[]> {
    const assignments: Assignment[] = await this.assignmentsDataService
      .getAllEmployeeAssignments(
        employeeId
      );

    assignments.forEach(async assignment => {
      const reqUpdateInfo: Partial<RequirementUpdateDTO> = await this
        .assignmentsDataService
        .getTotalCompletedWorkForReq(assignment.requirementId);

      await this.requirementsDataService.updateRequirement(
        assignment.requirementId, reqUpdateInfo
      );
    });

    return plainToClass(AssignmentReturnDTO, assignments, {
      excludeExtraneousValues: true,
    });
  }


  public async getAllRequirementAssignments(
    reqId: string,
  ): Promise<AssignmentReturnDTO[]> {
    const assignments: Assignment[] = await this.assignmentsDataService
      .getAllRequirementAssignments(
        reqId
      );

    const reqUpdateInfo = await this.assignmentsDataService
      .getTotalCompletedWorkForReq(reqId);

    await this.requirementsDataService.updateRequirement(reqId, reqUpdateInfo);

    return plainToClass(AssignmentReturnDTO, assignments, {
      excludeExtraneousValues: true,
    });
  }


  public async assignWorkToEmployee(
    employeeId: string,
    user: UserDisplayDTO,
    assignmentInfo: AssignmentCreateDTO,
  ): Promise<AssignmentReturnDTO> {
    const assignment: Assignment = await this.assignmentsDataService
      .assignWorkToEmployee(
        employeeId,
        user,
        assignmentInfo
      );

    const reqUpdateInfo: Partial<RequirementUpdateDTO> =
      new RequirementUpdateDTO();

    reqUpdateInfo.completedWorkTimePerDay = await this.assignmentsDataService
      .getTotalWorkInputPerDayForReq(assignment.requirementId);

    await this.requirementsDataService.updateRequirement(
      assignment.requirementId, reqUpdateInfo
    );

    return plainToClass(AssignmentReturnDTO, assignment, {
      excludeExtraneousValues: true,
    });
  }


  public async updateAssignment(
    assignmentId: string,
    employeeId: string,
    updateInfo: Partial<AssignmentUpdateDTO>,
  ): Promise<AssignmentReturnDTO> {
    const updatedAssignment: Assignment = await this.assignmentsDataService
      .updateAssignment(
        assignmentId,
        employeeId,
        updateInfo
      );

    const reqUpdateInfo: Partial<RequirementUpdateDTO> =
      new RequirementUpdateDTO();

    reqUpdateInfo.completedWorkTimePerDay = await this.assignmentsDataService
      .getTotalWorkInputPerDayForReq(updatedAssignment.requirementId);

    await this.requirementsDataService.updateRequirement(
      updatedAssignment.requirementId, reqUpdateInfo
    );

    return plainToClass(AssignmentReturnDTO, updatedAssignment, {
      excludeExtraneousValues: true
    });
  }
}
