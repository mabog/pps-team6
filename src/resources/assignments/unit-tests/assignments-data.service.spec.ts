import { Test, TestingModule } from '@nestjs/testing';
import { AssignmentsDataService } from '../assignments-data.service';

describe('AssignmentsDataService', () => {
  let service: AssignmentsDataService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AssignmentsDataService],
    }).compile();

    service = module.get<AssignmentsDataService>(AssignmentsDataService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
