import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  ParseUUIDPipe,
  Param,
  Post,
  Body,
  ValidationPipe,
  Patch,
  Delete,
  Req,
  UseGuards
} from '@nestjs/common';
import { AssignmentsDataService } from './assignments-data.service';
import {
  AssignmentReturnDTO
} from '../../models/assignments/assignment-return.dto';
import {
  AssignmentCreateDTO
} from '../../models/assignments/assignment-create.dto';
import {
  AssignmentUpdateDTO
} from '../../models/assignments/assignment-update.dto';
import { ResponseMessageDTO } from '../../models/common/response-message.dto';
import {
  AuthGuardWithBlacklisting
} from '../../middleware/guards/blacklist.guard';
import { UserDisplayDTO } from '../../models/users/user-display.dto';
import { AssignmentsService } from './assignments.service';

@Controller()
@UseGuards(AuthGuardWithBlacklisting)
export class AssignmentsController {
  public constructor(
    private readonly assignmentsDataService: AssignmentsDataService,
    private readonly assignmentsService: AssignmentsService,
  ) { }


  // All assignments for employee
  @Get('employees/:id/assignments')
  @HttpCode(HttpStatus.OK)
  public async getAssignmentsForEmployee(
    @Param('id', ParseUUIDPipe) employeeId: string,
  ): Promise<AssignmentReturnDTO[]> {

    return this.assignmentsService.getAllEmployeeAssignments(employeeId);
  }


  // All assignments for requirement
  @Get('requirements/:id/assignments')
  @HttpCode(HttpStatus.OK)
  public async getAssignmentsForRequirement(
    @Param('id', ParseUUIDPipe) reqId: string,
  ): Promise<AssignmentReturnDTO[]> {

    return this.assignmentsService.getAllRequirementAssignments(reqId);
  }


  // Add assignment to employee
  @Post('employees/:id/assignments')
  @HttpCode(HttpStatus.CREATED)
  public async assignWork(
    @Param('id', ParseUUIDPipe) employeeId: string,
    @Body(ValidationPipe) assignmentInfo: AssignmentCreateDTO,
    @Req() request: any,
  ): Promise<AssignmentReturnDTO> {
    const user: UserDisplayDTO = request.user;

    return await this
      .assignmentsService
      .assignWorkToEmployee(employeeId, user, assignmentInfo);
  }


  // Update assignment
  @Patch('employees/:employeeId/assignments/:id')
  @HttpCode(HttpStatus.OK)
  public async updateAssignment(
    @Param('id', ParseUUIDPipe) assignmentId: string,
    @Param('employeeId', ParseUUIDPipe) employeeId: string,
    @Body(ValidationPipe) updateInfo: Partial<AssignmentUpdateDTO>,
  ): Promise<AssignmentReturnDTO> {

    return await this
      .assignmentsService
      .updateAssignment(assignmentId, employeeId, updateInfo);
  }


  // Delete assignment
  @Delete('assignments/:id')
  @HttpCode(HttpStatus.OK)
  public async deleteAssignment(
    @Param('id', ParseUUIDPipe) assignmentId: string,
  ): Promise<ResponseMessageDTO> {

    return await this.assignmentsDataService.deleteAssignment(assignmentId);
  }

}
