import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  EmployeeDataRepository
} from '../../database/repositories/employee-data.repository';
import {
  AssignmentDataRepository
} from '../../database/repositories/assignment-data.repository';
import { Employee } from '../../database/entities/employee.entity';
import { Requirement } from '../../database/entities/requirement.entity';
import {
  RequirementDataRepository
} from '../../database/repositories/requirements-data.repository';
import {
  AssignmentCreateDTO
} from '../../models/assignments/assignment-create.dto';
import {
  ProjectSystemError
} from '../../middleware/exceptions/project-system.error';
import { Assignment } from '../../database/entities/assignment.entity';
import {
  AssignmentUpdateDTO
} from '../../models/assignments/assignment-update.dto';
import {
  UserDataRepository
} from '../../database/repositories/user-data.repository';
import { UserDisplayDTO } from '../../models/users/user-display.dto';
import { User } from '../../database/entities/user.entity';
import { ResponseMessageDTO } from '../../models/common/response-message.dto';
import { 
  RequirementUpdateDTO } from '../../models/requirements/requirement-update.dto';

@Injectable()
export class AssignmentsDataService {
  public constructor(
    @InjectRepository(AssignmentDataRepository)
    private readonly assignmentDataRepo: AssignmentDataRepository,
    @InjectRepository(EmployeeDataRepository)
    private readonly employeeDataRepo: EmployeeDataRepository,
    @InjectRepository(UserDataRepository)
    private readonly userDataRepo: UserDataRepository,
    @InjectRepository(RequirementDataRepository)
    private readonly reqDataRepo: RequirementDataRepository,
  ) { }


  public async getAllEmployeeAssignments(
    employeeId: string,
  ): Promise<Assignment[]> {
    const employeeEntity: Employee = await this
      .employeeDataRepo
      .findEmployeeEntityById(employeeId);

    return await this
      .assignmentDataRepo
      .getAllEmployeeAssignments(employeeEntity);
  }


  public async getAllRequirementAssignments(
    reqId: string,
  ): Promise<Assignment[]> {
    const reqEntity: Requirement = await this
      .reqDataRepo
      .findReqById(reqId);

    return await this
      .assignmentDataRepo
      .getAllRequirementAssignments(reqEntity);
  }


  public async assignWorkToEmployee(
    employeeId: string,
    user: UserDisplayDTO,
    assignmentInfo: AssignmentCreateDTO,
  ): Promise<Assignment> {
    const { employeeWorkInputPerDay } = assignmentInfo;

    const employeeEntity: Employee = await this
      .employeeDataRepo
      .findEmployeeEntityById(employeeId);

    const userEntity: User = await this
      .userDataRepo
      .findUserEntityByEmail(user.email);

    const reqEntity: Requirement = await this
      .reqDataRepo
      .findReqById(assignmentInfo.reqId);

    this.employeeDataRepo.canEmployeeWorkAssignment(
      employeeEntity,
      employeeWorkInputPerDay,
      reqEntity,
    );

    await this.assignmentDataRepo.isEmployeeAlreadyAssignedToRequirement(
      employeeEntity,
      assignmentInfo
    );

    const assignmentEntity: Assignment = await this
      .assignmentDataRepo
      .createAssignment(
        employeeEntity,
        userEntity,
        reqEntity,
        employeeWorkInputPerDay,
      );

    await this.reqDataRepo.addCompletedWorkTimePerDay(
      reqEntity, employeeWorkInputPerDay
    );

    await this.employeeDataRepo.deductAvailableWorkTime(
      employeeEntity,
      employeeWorkInputPerDay
    );

    return assignmentEntity;
  }


  public async updateAssignment(
    assignmentId: string,
    employeeId: string,
    updateInfo: Partial<AssignmentUpdateDTO>,
  ): Promise<Assignment> {
    const assignmentEntity: Assignment = await this
      .assignmentDataRepo
      .findAssignmentEntityById(assignmentId);
    const { employeeWorkInputPerDay } = assignmentEntity;

    const employeeEntity: Employee = await this
      .employeeDataRepo
      .findEmployeeEntityById(employeeId);

    if (assignmentEntity.assigneeId !== employeeId) {
      await this.employeeDataRepo.reassignWork(
        assignmentEntity.assigneeId,
        employeeEntity,
        employeeWorkInputPerDay
      );
    }

    await this.employeeDataRepo.updateWorkInput(
      employeeEntity,
      employeeWorkInputPerDay,
      updateInfo.employeeWorkInputPerDay
    );

    this.assignmentDataRepo.updateTotalTimeWorked(assignmentEntity);

    return await this
      .assignmentDataRepo
      .updateAssignment(
        assignmentEntity, employeeEntity, updateInfo
      );
  }


  public async deleteAssignment(
    assignmentId: string,
  ): Promise<ResponseMessageDTO> {
    const assignmentEntity: Assignment = await this
      .assignmentDataRepo
      .findAssignmentEntityById(assignmentId);
    const { employeeWorkInputPerDay } = assignmentEntity;

    const employeeEntity: Employee = await this
      .employeeDataRepo
      .findEmployeeEntityById(assignmentEntity.assigneeId);

    const reqEntity: Requirement = await this
      .reqDataRepo
      .findReqById(assignmentEntity.requirementId);

    await this.assignmentDataRepo.deleteAssignment(
      assignmentEntity,
    );

    if (assignmentEntity.isDeleted) {
      await this.employeeDataRepo.restoreAvailableWorkTime(
        employeeEntity,
        employeeWorkInputPerDay
      );

      await this.reqDataRepo.deductCompletedWorkTimePerDay(
        reqEntity,
        employeeWorkInputPerDay
      );

      return { msg: 'Assignment deleted' };
    } else {
      throw new ProjectSystemError('Assignment does not exist', 404);
    }
  }


  public async getTotalCompletedWorkForReq(
    reqId: string
  ): Promise<Partial<RequirementUpdateDTO>> {
    const reqUpdateInfo: Partial<RequirementUpdateDTO> =
      new RequirementUpdateDTO();

    reqUpdateInfo.totalCompletedWorkTime = await this.assignmentDataRepo
      .getTotalCompletedWorkForReq(reqId);

    return reqUpdateInfo;
  }


  public async getTotalWorkInputPerDayForReq(reqId: string) {

    return await this.assignmentDataRepo.getTotalWorkInputPerDayForReq(reqId);
  }
}
