import { Module, forwardRef } from '@nestjs/common';
import { AssignmentsDataService } from './assignments-data.service';
import { AssignmentsController } from './assignments.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  RequirementDataRepository
} from '../../database/repositories/requirements-data.repository';
import {
  EmployeeDataRepository
} from '../../database/repositories/employee-data.repository';
import {
  UserDataRepository
} from '../../database/repositories/user-data.repository';
import {
  AssignmentDataRepository
} from '../../database/repositories/assignment-data.repository';
import { AssignmentsService } from './assignments.service';
import { RequirementsModule } from '../requirements/requirements.module';

@Module({
  imports:
    [
      forwardRef(() => RequirementsModule),
      TypeOrmModule.forFeature(
        [
          RequirementDataRepository,
          EmployeeDataRepository,
          UserDataRepository,
          AssignmentDataRepository
        ]
      )],
  providers: [AssignmentsDataService, AssignmentsService],
  controllers: [AssignmentsController],
  exports: [AssignmentsDataService, AssignmentsService]
})
export class AssignmentsModule { }
