import { Test, TestingModule } from '@nestjs/testing';
import { SkillsDataService } from '../skills-data.service';

describe('SkillsService', () => {
  let service: SkillsDataService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SkillsDataService],
    }).compile();

    service = module.get<SkillsDataService>(SkillsDataService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
