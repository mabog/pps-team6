import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Query,
  ValidationPipe,
  Post,
  Body,
  UseGuards
} from '@nestjs/common';
import { SkillsDataService } from './skills-data.service';
import { Skill } from '../../database/entities/skill.entity';
import { SkillCreateDTO } from '../../models/skills/skill-create.dto';
import { AdminGuard } from '../../middleware/guards/admin.guard';
import { AuthGuardWithBlacklisting } from '../../middleware/guards/blacklist.guard';

@Controller('skills')
@UseGuards(AuthGuardWithBlacklisting)
export class SkillsController {
  public constructor(
    private readonly skillsDataService: SkillsDataService
  ) { }

  // All skills
  @Get()
  @HttpCode(HttpStatus.OK)
  public async getAllSkills(
    @Query('skill', ValidationPipe) skill: string
  ): Promise<Skill[]> {

    const skills = await this.skillsDataService.getAllSkills();

    if (skill) {
      return skills.filter(x =>
        x.skillName.toLowerCase().includes(skill.toLowerCase()),
      );
    }

    return skills;
  }


  // Add skill
  @Post()
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AdminGuard)
  public async createNewSkill(
    @Body() newSkillName: SkillCreateDTO,
  ) {

    return this.skillsDataService.createNewSkill(newSkillName);
  }
}
