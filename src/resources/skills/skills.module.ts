import { Module } from '@nestjs/common';
import { SkillsController } from './skills.controller';
import { SkillsDataService } from './skills-data.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  SkillDataRepository
} from '../../database/repositories/skill-data.repository';

@Module({
  imports: [TypeOrmModule.forFeature(
    [
      SkillDataRepository,
    ]
  )],
  controllers: [SkillsController],
  providers: [SkillsDataService]
})
export class SkillsModule { }
