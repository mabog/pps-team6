import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Skill } from '../../database/entities/skill.entity';
import {
  SkillDataRepository
} from '../../database/repositories/skill-data.repository';
import { SkillCreateDTO } from '../../models/skills/skill-create.dto';

@Injectable()
export class SkillsDataService {
  public constructor(
    @InjectRepository(SkillDataRepository)
    private readonly skillsDataRepo: SkillDataRepository,
  ) { }

  public async getAllSkills(): Promise<Skill[]> {

    return await this.skillsDataRepo.find();
  }

  public async createNewSkill(newSkillName: SkillCreateDTO) {

    return this.skillsDataRepo.addNewSkill(newSkillName);
  }

}
