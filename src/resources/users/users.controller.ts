import {
  Controller,
  Post,
  HttpCode,
  HttpStatus,
  Body,
  ValidationPipe,
  UseGuards,
  Get,
  Param,
  ParseUUIDPipe,
  Delete,
  UseInterceptors,
  UploadedFile,
  Patch,
  Put,
  Req,
} from '@nestjs/common';
import { UsersDataService } from './users-data.service';
import { UserCreateDTO } from '../../models/users/user-create.dto';
import { UserReturnDTO } from '../../models/users/user-return.dto';
import { UserPosition } from '../../models/users/enums/user-position.enum';
import {
  AuthGuardWithBlacklisting
} from '../../middleware/guards/blacklist.guard';
import { AdminGuard } from '../../middleware/guards/admin.guard';
import { ResponseMessageDTO } from '../../models/common/response-message.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { UserUpdateDTO } from '../../models/users/user-update.dto';
import { UsersService } from './users.service';
import { EmployeeReturnDTO } from '../../models/employees/employee-return.dto';
import { ProjectReturnDTO } from '../../models/projects/project-return.dto';
import { UserDisplayDTO } from '../../models/users/user-display.dto';


@Controller('users')
@UseGuards(AuthGuardWithBlacklisting)
export class UsersController {
  public constructor(
    private readonly usersDataService: UsersDataService,
    private readonly usersService: UsersService,
  ) { }

  // Register user
  @Post('/register')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AdminGuard)
  public async signUp(
    @Body(ValidationPipe) userInfo: UserCreateDTO,
    @Body(ValidationPipe) skills: string,
  ): Promise<UserReturnDTO> {

    return await this.usersService.createUser(
      userInfo, skills['skills'], UserPosition.REGULAR,
    );
  }

  
  // All users
  @Get()
  @HttpCode(HttpStatus.OK)
  public async getAllUsers(): Promise<UserReturnDTO[]> {
    
    return await this.usersDataService.getAllUsers();
  }

  
  // Logged user info
  @Get('/me')
  @HttpCode(HttpStatus.OK)
  public async getLoggedUserInfo(
  @Req() request,
  ): Promise<UserReturnDTO> {
    const user: UserDisplayDTO = request.user;
    
    return await this.usersService.getLoggedUserInfo(user);
  }


  // Single user
  @Get('/:id')
  @HttpCode(HttpStatus.OK)
  public async getSingleUser(
    @Param('id', ParseUUIDPipe) userId: string,
  ): Promise<UserReturnDTO> {

    return await this.usersDataService.getSingleUser(userId);
  }


  // All employee subordinates
  @Get('/:id/employees')
  @HttpCode(HttpStatus.OK)
  public async getAllEmployeeSubordinates(
    @Param('id', ParseUUIDPipe) userId: string,
  ): Promise<EmployeeReturnDTO[]> {

    return await this.usersService.getAllEmployeeSubordinates(userId);
  }


  // All manager subordinates
  @Get('/:id/users')
  @HttpCode(HttpStatus.OK)
  public async getAllUserSubordinates(
    @Param('id', ParseUUIDPipe) userId: string,
  ): Promise<UserReturnDTO[]> {

    return await this.usersService.getAllUserSubordinates(userId);
  }


  // All projects for manager
  @Get('/:id/projects')
  @HttpCode(HttpStatus.OK)
  public async getProjectsByManager(
    @Param('id', ParseUUIDPipe) managerID: string
  ): Promise<ProjectReturnDTO[]> {

    return this.usersService.getAllUserProjects(managerID);
  }


  // Update employee
  @Patch('/:id')
  @HttpCode(HttpStatus.OK)
  public async updateEmployee(
    @Param('id', ParseUUIDPipe) employeeId: string,
    @Body() userUpdateInfo: Partial<UserUpdateDTO>,
  ): Promise<UserReturnDTO> {

    return this.usersService.updateUser(
      employeeId, userUpdateInfo
    );
  }


  // Assign user to manager
  @Put('/:id/users/:managerId')
  @HttpCode(HttpStatus.OK)
  public async assignUserToManager(
    @Param('id', ParseUUIDPipe) userId: string,
    @Param('managerId', ParseUUIDPipe) managerId: string,
  ): Promise<UserReturnDTO> {

    return this.usersService
      .assignUserToManager(
        userId,
        managerId,
      );
  }


  // Assign employee to manager
  @Put('/:id/employees/:employeeId')
  @HttpCode(HttpStatus.OK)
  public async assignEmployeeToManager(
    @Param('id', ParseUUIDPipe) userId: string,
    @Param('employeeId', ParseUUIDPipe) employeeId: string,
  ): Promise<EmployeeReturnDTO> {

    return this.usersService
      .assignEmployeeToManager(
        userId,
        employeeId
      );
  }


  // Delete user
  @Delete('/:id')
  @HttpCode(HttpStatus.OK)
  public async deleteUser(
    @Param('id', ParseUUIDPipe) userId: string,
  ): Promise<ResponseMessageDTO> {
    await this.usersDataService.deleteUser(userId);

    return { msg: 'User deleted' }
  }


  // Upload avatar
  @Post('/:id/avatar')
  @HttpCode(HttpStatus.CREATED)
  @UseInterceptors(FileInterceptor('file'))
  @UseGuards(AuthGuardWithBlacklisting)
  public async uploadAvatar(
    @Param('id', ParseUUIDPipe) userId: string,
    @UploadedFile() file: any
  ): Promise<UserReturnDTO> {

    const updateUsersAvatar: Partial<UserUpdateDTO> = {
      avatarUrl: file.filename,
    }

    return await this.usersService.updateUser(userId, updateUsersAvatar);
  }

}
