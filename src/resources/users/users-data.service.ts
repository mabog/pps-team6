import {
  UserDataRepository
} from '../../database/repositories/user-data.repository';
import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserReturnDTO } from '../../models/users/user-return.dto';
import { UserPosition } from '../../models/users/enums/user-position.enum';
import { UserCreateDTO } from '../../models/users/user-create.dto';
import { Position } from '../../database/entities/position.entity';
import { Repository, In } from 'typeorm';
import { plainToClass } from 'class-transformer';
import { User } from '../../database/entities/user.entity';
import { UserUpdateDTO } from '../../models/users/user-update.dto';
import {
  SkillDataRepository
} from '../../database/repositories/skill-data.repository';
import { Skill } from '../../database/entities/skill.entity';
import {
  ProjectSystemError
} from '../../middleware/exceptions/project-system.error';
import {
  EmployeeDataRepository
} from '../../database/repositories/employee-data.repository';
import { Employee } from '../../database/entities/employee.entity';
import {
  ProjectDataRepository
} from '../../database/repositories/project-data.repository';
import { Project } from '../../database/entities/project.entity';
import { UserDisplayDTO } from '../../models/users/user-display.dto';


@Injectable()
export class UsersDataService {
  public constructor(
    @InjectRepository(Position)
    private readonly positionsRepo: Repository<Position>,
    @InjectRepository(SkillDataRepository)
    private readonly skillsDataRepo: SkillDataRepository,
    @InjectRepository(UserDataRepository)
    private readonly userDataRepo: UserDataRepository,
    @InjectRepository(EmployeeDataRepository)
    private readonly employeeDataRepo: EmployeeDataRepository,
    @InjectRepository(ProjectDataRepository)
    private readonly projectDataRepo: ProjectDataRepository,
  ) { }


  public async getAllUsers(): Promise<UserReturnDTO[]> {
    const users: User[] = await this.userDataRepo.find({
      isDeleted: false,
    });

    return plainToClass(UserReturnDTO, users, {
      excludeExtraneousValues: true,
    });
  }


  public async getSingleUser(id: string): Promise<UserReturnDTO> {
    const user: User = await this.userDataRepo.findUserEntityById(id);

    return plainToClass(UserReturnDTO, user, {
      excludeExtraneousValues: true,
    });
  }


  public async getLoggedUser(user: UserDisplayDTO): Promise<User> {

    return await this.userDataRepo.findUserEntityByEmail(user.email)
  }


  public async getAllEmployeeSubordinates(
    userId: string
  ): Promise<Employee[]> {
    const employeeSubordinates: Employee[] = await this.employeeDataRepo.find({
      where: {
        user: userId
      }
    });

    if (employeeSubordinates.length === 0) {
      throw new ProjectSystemError(
        'User has no Employee subordinates under him', 404
      );
    }

    return employeeSubordinates;
  }


  public async getAllUserSubordinates(
    userId: string
  ): Promise<User[]> {
    const userSubordinates: User[] = await this.userDataRepo.find({
      where: {
        managedBy: userId,
      }
    });

    if (userSubordinates.length === 0) {
      throw new ProjectSystemError(
        'User has no Manager subordinates under him', 404
      );
    }

    return userSubordinates;
  }


  public async getAllUserProjects(
    userId: string
  ): Promise<Project[]> {

    return await this
      .projectDataRepo
      .findProjectsByManager(userId);
  }


  public async createUser(
    userInfo: UserCreateDTO,
    skills: string,
    ...positions: UserPosition[]
  ): Promise<User> {
    try {
      const positionsToAssign: Position[] = await this.positionsRepo.find({
        where: {
          positionName: In(positions)
        },
      });

      const skillsToAssign: string[] = skills.split(',');
      skillsToAssign.splice(0, 0, 'MANAGEMENT');

      const userSkills: Skill[] = await this.skillsDataRepo.find({
        where: {
          skillName: In(skillsToAssign),
        }
      });

      const createdUser: User = await this.userDataRepo.createUser(
        userInfo, positionsToAssign, userSkills
      );

      if (userInfo.managedById) {
        await this.assignUserToManager(
          createdUser.id, userInfo.managedById
        );
      }

      return createdUser;
    } catch (error) {
      if (error.errno === 1062) {
        throw new ProjectSystemError(
          'User with such e-mail already exists', 409
        );
      } else {
        throw new InternalServerErrorException()
      }
    }
  }


  public async deleteUser(id: string): Promise<void> {
    const userEntity: User = await this.userDataRepo.findUserEntityById(id);

    await this.userDataRepo.save({
      ...userEntity,
      isDeleted: true,
    });
  }


  public async updateUser(
    id: string,
    userInfo: Partial<UserUpdateDTO>
  ): Promise<User> {
    const foundUser: User = await this.userDataRepo.findUserEntityById(id);
    const userToUpdate: User = { ...foundUser, ...userInfo };

    return await this.userDataRepo.save(userToUpdate);
  }


  public async assignUserToManager(
    userId: string,
    managerId: string,
  ): Promise<User> {
    const userEntity: User = await this.userDataRepo
      .findUserEntityById(userId);

    const managerEntity: User = await this.userDataRepo
      .findUserEntityById(managerId);

    return await this.userDataRepo.assignUserToManager(
      userEntity, managerEntity
    );
  }


  public async assignEmployeeToManager(
    userId: string,
    employeeId: string,
  ): Promise<Employee> {
    const userEntity: User = await this.userDataRepo
      .findUserEntityById(userId);

    const employeeEntity: Employee = await this
      .employeeDataRepo
      .findEmployeeEntityById(employeeId);

    return await this.employeeDataRepo.assignEmployeeToManager(
      userEntity,
      employeeEntity
    );
  }
}
