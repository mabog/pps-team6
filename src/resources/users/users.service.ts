import { Injectable } from '@nestjs/common';
import { UsersDataService } from './users-data.service';
import { UserCreateDTO } from '../../models/users/user-create.dto';
import { UserPosition } from '../../models/users/enums/user-position.enum';
import { UserReturnDTO } from '../../models/users/user-return.dto';
import { plainToClass } from 'class-transformer';
import { User } from '../../database/entities/user.entity';
import { Employee } from '../../database/entities/employee.entity';
import { EmployeeReturnDTO } from '../../models/employees/employee-return.dto';
import { ProjectReturnDTO } from '../../models/projects/project-return.dto';
import { Project } from '../../database/entities/project.entity';
import { UserUpdateDTO } from '../../models/users/user-update.dto';
import { UserDisplayDTO } from '../../models/users/user-display.dto';

@Injectable()
export class UsersService {
  public constructor(
    private readonly usersDataService: UsersDataService
  ) { }

  public async createUser(
    userInfo: UserCreateDTO,
    skills: string,
    positions: UserPosition,
  ): Promise<UserReturnDTO> {
    const createdUser: User = await this.usersDataService.createUser(
      userInfo, skills, positions,
    );

    return plainToClass(UserReturnDTO, createdUser, {
      excludeExtraneousValues: true,
    });
  }


  public async getAllEmployeeSubordinates(
    userId: string
  ): Promise<EmployeeReturnDTO[]> {
    const employeeSubordinates: Employee[] = await this.usersDataService
      .getAllEmployeeSubordinates(userId);

    return plainToClass(EmployeeReturnDTO, employeeSubordinates, {
      excludeExtraneousValues: true,
    });
  }


  public async getAllUserSubordinates(
    userId: string
  ): Promise<UserReturnDTO[]> {
    const userSubordinates: User[] = await this.usersDataService
      .getAllUserSubordinates(userId);

    return plainToClass(UserReturnDTO, userSubordinates, {
      excludeExtraneousValues: true,
    });
  }


  public async getAllUserProjects(
    userId: string
  ): Promise<ProjectReturnDTO[]> {
    const projects: Project[] = await this
      .usersDataService
      .getAllUserProjects(userId);

    return plainToClass(ProjectReturnDTO, projects, {
      excludeExtraneousValues: true
    });
  }


  public async getLoggedUserInfo(
    user: UserDisplayDTO,
  ): Promise<UserReturnDTO> {
    const userEntity: User = await this
    .usersDataService
    .getLoggedUser(user);

    return plainToClass(UserReturnDTO, userEntity, {
      excludeExtraneousValues: true,
    });
  }


  public async updateUser(
    userId: string,
    userInfo: Partial<UserUpdateDTO>
  ): Promise<UserReturnDTO> {
    const updatedUser: User = await this.usersDataService.updateUser(
      userId,
      userInfo,
    );

    return plainToClass(UserReturnDTO, updatedUser, {
      excludeExtraneousValues: true,
    });
  }


  public async assignUserToManager(
    userId: string,
    managerId: string,
  ): Promise<UserReturnDTO> {
    const userEntity: User = await this.usersDataService
      .assignUserToManager(
        userId,
        managerId,
      );

    return plainToClass(UserReturnDTO, userEntity, {
      excludeExtraneousValues: true,
    });
  }


  public async assignEmployeeToManager(
    userId: string,
    employeeId: string,
  ): Promise<EmployeeReturnDTO> {
    const employeeEntity: Employee = await this.usersDataService
      .assignEmployeeToManager(
        userId,
        employeeId,
      );

    return plainToClass(EmployeeReturnDTO, employeeEntity, {
      excludeExtraneousValues: true,
    });
  }

}
