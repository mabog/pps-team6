import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersDataService } from './users-data.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  UserDataRepository
} from '../../database/repositories/user-data.repository';
import { Position } from '../../database/entities/position.entity';
import {
  ProjectDataRepository
} from '../../database/repositories/project-data.repository';
import { UsersService } from './users.service';
import {
  SkillDataRepository
} from '../../database/repositories/skill-data.repository';
import {
  EmployeeDataRepository
} from '../../database/repositories/employee-data.repository';

@Module({
  imports: [TypeOrmModule.forFeature(
    [
      Position,
      UserDataRepository,
      EmployeeDataRepository,
      ProjectDataRepository,
      SkillDataRepository,
    ]
  )],
  controllers: [UsersController],
  providers: [UsersDataService, UsersService],
  exports: [UsersDataService]
})
export class UserModule { }
