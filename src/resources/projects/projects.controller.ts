import {
  Controller,
  Post,
  HttpCode,
  UseGuards,
  Body,
  HttpStatus,
  ValidationPipe,
  Req,
  Patch,
  Param,
  ParseUUIDPipe,
  Delete,
  Get,
} from '@nestjs/common';
import { ProjectCreateDTO } from './../../models/projects/project-create.dto';
import { ProjectUpdateDTO } from '../../models/projects/project-update.dto';
import { ProjectReturnDTO } from '../../models/projects/project-return.dto';
import {
  AuthGuardWithBlacklisting
} from '../../middleware/guards/blacklist.guard';
import { ResponseMessageDTO } from '../../models/common/response-message.dto';
import { ProjectsService } from './projects.service';
import {
  RequirementReturnDTO
} from '../../models/requirements/requirement-return.dto';

@Controller('projects')
@UseGuards(AuthGuardWithBlacklisting)
export class ProjectsController {

  public constructor(
    private readonly projectsService: ProjectsService,
  ) { }

  // Get all projects
  @Get()
  @HttpCode(HttpStatus.OK)
  public async getAllProjects(): Promise<ProjectReturnDTO[]> {

    return await this.projectsService.getAllProjects();
  }

// Get single project
  @Get('/:id')
  public async getSingleProject(
    @Param('id', ParseUUIDPipe) projectId: string
  ): Promise<ProjectReturnDTO> {

    return this.projectsService.getSingleProject(projectId);
  }

// Get all reqs for project
  @Get('/:id/requirements')
  @HttpCode(HttpStatus.OK)
  public async getAllReqsForProject(
    @Param('id', ParseUUIDPipe) projectId: string,
  ): Promise<RequirementReturnDTO[]> {

    return await this.projectsService.getAllProjectRequirements(projectId);
  }

// Create project
  @Post('/create')
  @HttpCode(HttpStatus.CREATED)
  public async createProject(
    @Body(ValidationPipe) projectInfo: ProjectCreateDTO,
    @Req() request: any
  ): Promise<ProjectReturnDTO> {

    return await this.projectsService.createProject(
      projectInfo, request.user.email
    );
  }

// Update project
  @Patch('/:id')
  @HttpCode(HttpStatus.OK)
  public async updateProject(
    @Param('id', ParseUUIDPipe) projectId: string,
    @Body(ValidationPipe) projectUpdateInfo: Partial<ProjectUpdateDTO>
  ): Promise<ProjectReturnDTO> {

    return this.projectsService.updateProject(
      projectId, projectUpdateInfo
    );
  }

// Update project status
  @Patch('/:id/status')
  @HttpCode(HttpStatus.OK)
  public async updateProjectStatus(
    @Param('id', ParseUUIDPipe) projectId: string,
    @Body() projectStatusUpdate: string,
  ): Promise<ProjectReturnDTO> {

    return await this.projectsService
      .updateProjectStatus(
        projectId,
        projectStatusUpdate['status']
      );
  }

// Reassign project
  @Patch('/:id/users/:userId')
  @HttpCode(HttpStatus.OK)
  public async reassignProject(
    @Param('id', ParseUUIDPipe) projectId: string,
    @Param('userId', ParseUUIDPipe) userId: string,
  ): Promise<ProjectReturnDTO> {

    return await this.projectsService
      .reassignProject(
        projectId,
        userId,
      );
  }

// Delete project
  @Delete('/:id')
  @HttpCode(HttpStatus.OK)
  public async deleteProject(
    @Param('id', ParseUUIDPipe) projectId: string
  ): Promise<ResponseMessageDTO> {
    await this.projectsService.deleteProject(projectId);

    return { msg: 'Project deleted' }
  }

}
