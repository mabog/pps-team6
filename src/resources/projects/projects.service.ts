import { Injectable } from '@nestjs/common';
import { ProjectsDataService } from './projects-data.service';
import { Project } from '../../database/entities/project.entity';
import {
  ProjectSystemError
} from '../../middleware/exceptions/project-system.error';
import { RequirementsService } from '../requirements/requirements.service';
import { ProjectReturnDTO } from '../../models/projects/project-return.dto';
import { plainToClass } from 'class-transformer';
import { Requirement } from '../../database/entities/requirement.entity';
import {
  AssignmentsDataService
} from '../assignments/assignments-data.service';
import {
  RequirementsDataService
} from '../requirements/requirements-data.service';
import {
  RequirementUpdateDTO
} from '../../models/requirements/requirement-update.dto';
import {
  RequirementReturnDTO
} from '../../models/requirements/requirement-return.dto';
import { ProjectUpdateDTO } from '../../models/projects/project-update.dto';
import { ProjectCreateDTO } from '../../models/projects/project-create.dto';
import { ProjectStatus } from '../../models/projects/enum/project-status.enum';

@Injectable()
export class ProjectsService {
  public constructor(
    private readonly projectsDataService: ProjectsDataService,
    private readonly assignmentsDataService: AssignmentsDataService,
    private readonly requirementsDataService: RequirementsDataService,

    // This coupling needs to be removed in the future
    private readonly requirementsService: RequirementsService,
  ) { }


  public async getSingleProject(projectId: string): Promise<ProjectReturnDTO> {
    const projectEntity: Project = await this.projectsDataService
      .getSingleProject(projectId);

    return plainToClass(ProjectReturnDTO, projectEntity, {
      excludeExtraneousValues: true
    });
  }

  public async getAllProjects(): Promise<ProjectReturnDTO[]> {
    const projects: Project[] = await this.projectsDataService
      .getAllProjects()

    return plainToClass(ProjectReturnDTO, projects, {
      excludeExtraneousValues: true
    });
  }


  public async getAllProjectRequirements(
    projectId: string
  ) {
    const projectEntity: Project = await this.projectsDataService
      .getSingleProject(projectId);

    const reqsEntity: Requirement[] = await projectEntity.requirements;

    // Updates Requirements based on the current state of all Assignments
    reqsEntity.forEach(async requirement => {
      const reqUpdateInfo: Partial<RequirementUpdateDTO> = await this
        .assignmentsDataService
        .getTotalCompletedWorkForReq(
          requirement.id
        );

      await this.requirementsDataService.updateRequirement(
        requirement.id, reqUpdateInfo
      );
    });

    return plainToClass(RequirementReturnDTO, reqsEntity, {
      excludeExtraneousValues: true,
    });
  }


  public async createProject(
    projectInfo: ProjectCreateDTO,
    email: string,
  ): Promise<ProjectReturnDTO> {

    if (Number(projectInfo.managerWorkInputPerDay) === 0) {
      throw new ProjectSystemError(
        'Cannot create a Project with 0 hours WorkInputPerDay', 409
      );
    }

    const createdProject: Project = await this
      .projectsDataService
      .createProject(projectInfo, email);

    return plainToClass(ProjectReturnDTO, createdProject, {
      excludeExtraneousValues: true
    });
  }


  public async updateProject(
    projectId: string,
    projectUpdateInfo: Partial<ProjectUpdateDTO>
  ): Promise<ProjectReturnDTO> {
    const updatedProject: Project = await this
      .projectsDataService
      .updateProject(projectId, projectUpdateInfo);

    return plainToClass(ProjectReturnDTO, updatedProject, {
      excludeExtraneousValues: true
    });
  }


  public async updateProjectStatus(
    projectId: string,
    projectStatusUpdate: string
  ): Promise<ProjectReturnDTO> {

    const isCorrectStatus: boolean[] = this.allowedProjectStatus
      .map(status => status === projectStatusUpdate.toUpperCase());

    if (!isCorrectStatus.includes(true)) {
      throw new ProjectSystemError(
        `${projectStatusUpdate} is an invalid project status`, 409
      );
    }

    const updatedProject: Project = await this.projectsDataService
      .updateProjectStatus(
        projectId,
        projectStatusUpdate
      );

    return plainToClass(ProjectReturnDTO, updatedProject, {
      excludeExtraneousValues: true
    });
  }


  public async reassignProject(
    projectId: string,
    userId: string,
  ): Promise<ProjectReturnDTO> {
    const projectEntity: Project = await this.projectsDataService
      .reassignProject(
        projectId,
        userId,
      );

    return plainToClass(ProjectReturnDTO, projectEntity, {
      excludeExtraneousValues: true
    });

  }


  public async deleteProject(projectId: string) {
    try {
      const projectEntity: Project = await this
        .projectsDataService
        .deleteProject(projectId);

      if (projectEntity.isDeleted) {
        const requirementsIds: string[] = (await projectEntity.requirements)
          .map(req => {
            if (!req.isDeleted) {

              return req.id;
            }
          });

        requirementsIds.forEach(
          id => this.requirementsService.deleteRequirement(id)
        );
      }
    } catch (error) {
      throw new ProjectSystemError(error, 404);
    }
  }


  private readonly allowedProjectStatus: ProjectStatus[] = [
    ProjectStatus.IN_PROGRESS,
    ProjectStatus.STOPPED,
    ProjectStatus.COMPLETED
  ];
}
