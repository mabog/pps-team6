  import { Project } from '../../database/entities/project.entity';
import { User } from '../../database/entities/user.entity';
import { ProjectCreateDTO } from '../../models/projects/project-create.dto';
import { Injectable } from '@nestjs/common';
import {
  ProjectDataRepository
} from '../../database/repositories/project-data.repository';
import { InjectRepository } from '@nestjs/typeorm';
import {
  UserDataRepository
} from '../../database/repositories/user-data.repository';
import { ProjectUpdateDTO } from '../../models/projects/project-update.dto';
import {
  RequirementDataRepository
} from '../../database/repositories/requirements-data.repository';
import {
  RequirementLongestCompletionDateDTO
} from '../../models/requirements/requirement-longest-completion-date.dto';
import {
  ProjectSystemError
} from '../../middleware/exceptions/project-system.error';
import { ProjectStatus } from '../../models/projects/enum/project-status.enum';
import moment = require('moment');

@Injectable()
export class ProjectsDataService {

  public constructor(
    @InjectRepository(ProjectDataRepository)
    private readonly projectDataRepo: ProjectDataRepository,
    @InjectRepository(UserDataRepository)
    private readonly userDataRepository: UserDataRepository,
    @InjectRepository(RequirementDataRepository)
    private readonly requirementDataRepository: RequirementDataRepository,
  ) { }


  public async getSingleProject(projectId: string): Promise<Project> {
    const projectEntity: Project = await this.projectDataRepo
      .findProjectByID(projectId);

    await this.updateProjectCompletionDayAndDate(projectEntity);
    await this.isProjectWithinTarget(projectEntity);

    return projectEntity;
  }

  
  public async getAllProjects(): Promise<Project[]> {
    const projects: Project[] = await this.projectDataRepo.find({
      isDeleted: false,
    });

    projects.map(async projectEntity => {
      await this.updateProjectCompletionDayAndDate(projectEntity);
      await this.isProjectWithinTarget(projectEntity);
    });

    return projects
  }


  public async createProject(
    projectInfo: ProjectCreateDTO,
    email: string,
  ): Promise<Project> {
    const projectManager: User = await this
      .userDataRepository
      .findUserEntityByEmail(email);

    const { managerWorkInputPerDay } = projectInfo;

    if (projectManager.totalAvailableWorkTime <= 0) {
      throw new ProjectSystemError(
        'Manager does not have available work hours to create project', 409
      );
    }

    this.userDataRepository
      .canUserWorkProject(
        projectManager, managerWorkInputPerDay
      );

    const createdProject: Project = await this
      .projectDataRepo
      .createProject(projectInfo, projectManager);

    await this.userDataRepository
      .deductAvailableWorkTime(
        projectManager, managerWorkInputPerDay
      );

    return createdProject;
  }


  public async updateProject(
    projectId: string,
    updateInfo: Partial<ProjectUpdateDTO>
  ): Promise<Project> {
    const projectEntity: Project = await this
      .projectDataRepo
      .findProjectByID(projectId);

    const projectManagerId: string = (await projectEntity.user).id;

    if (updateInfo.managerWorkInputPerDay) {
      const managerCurrentWorkInputPerDay = projectEntity.managerWorkInputPerDay;

      await this.userDataRepository
        .updateWorkInput(
          projectManagerId,
          managerCurrentWorkInputPerDay,
          updateInfo.managerWorkInputPerDay
        );
    }

    return await this.projectDataRepo
      .updateProject(projectEntity, updateInfo);
  }


  public async updateProjectStatus(
    projectId: string,
    projectStatusUpdate: string
  ): Promise<Project> {
    const projectEntity: Project = await this.projectDataRepo
      .findProjectByID(projectId);

    const userEntity: User = await this.userDataRepository
      .findUserEntityById((await projectEntity.user).id);

    if (projectStatusUpdate === ProjectStatus.COMPLETED) {
      await this.projectDataRepo.completeProject(projectEntity);

      await this.userDataRepository.restoreAvailableWorkTime(
        userEntity,
        projectEntity.managerWorkInputPerDay
      );
    }

    projectEntity.status = projectStatusUpdate;

    return await this.projectDataRepo.save(projectEntity);
  }


  public async reassignProject(
    projectId: string,
    userId: string,
  ): Promise<Project> {
    const projectEntity: Project = await this.projectDataRepo
      .findProjectByID(projectId);

    const userEntity: User = await this.userDataRepository
      .findUserEntityById(userId);

    const workInputPerDay: number = projectEntity.managerWorkInputPerDay;

    if ((await projectEntity.user).id === userEntity.id) {
      throw new ProjectSystemError(
        'User is already assigned to this project', 409
      );
    }

    await this.userDataRepository.recalculateWorkHours(
      (await projectEntity.user).id,
      userEntity,
      workInputPerDay
    );

    return await this.projectDataRepo
      .reassignProject(projectEntity, userEntity);
  }


  public async updateProjectCompletionDayAndDate(
    projectEntity: Project
  ): Promise<Project> {
    const longestRequirement: RequirementLongestCompletionDateDTO = await this
      .requirementDataRepository
      .getRequirementsLongestCompletionDayAndDate(projectEntity.id);

      if (!longestRequirement) {
        projectEntity.daysUntilCompletion = 0;
        projectEntity.estCompletionDate = moment(
          new Date().setFullYear(2050)).format('YYYY/MM/DD');
  
        return projectEntity;
      }
  
    const { daysUntilCompletion, estCompletionDate } = longestRequirement;

    projectEntity.daysUntilCompletion = daysUntilCompletion;
    projectEntity.estCompletionDate = estCompletionDate;
    await this.isProjectWithinTarget(projectEntity);

    return projectEntity;
  }


  public async deleteProject(id: string): Promise<Project> {
    const projectEntity: Project = await this
      .projectDataRepo
      .findProjectByID(id);

    const userEntity: User = await this
      .userDataRepository
      .findUserEntityById((await projectEntity.user).id);

    const { managerWorkInputPerDay } = projectEntity;

    await this.projectDataRepo.deleteProject(projectEntity);

    if (projectEntity.isDeleted) {
      await this.userDataRepository.restoreAvailableWorkTime(
        userEntity,
        managerWorkInputPerDay
      );
    }

    return projectEntity;
  }


  public async isProjectWithinTarget(
    projectEntity: Project
  ): Promise<Project> {
    if (projectEntity.daysUntilCompletion > projectEntity.targetInDays ||
      (projectEntity.daysUntilCompletion === 0 && !projectEntity.isCompleted)
    ) {
      projectEntity.isWithinTarget = false;
    } else {
      projectEntity.isWithinTarget = true;
    }

    return await this.projectDataRepo.save(projectEntity);
  }

}
