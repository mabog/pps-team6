import { Module } from '@nestjs/common';
import { ProjectsDataService } from './projects-data.service';
import { ProjectsController } from './projects.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from '../users/users.module';
import {
  UserDataRepository
} from './../../database/repositories/user-data.repository';
import {
  ProjectDataRepository
} from '../../database/repositories/project-data.repository';
import { ProjectsService } from './projects.service';
import { RequirementsModule } from '../requirements/requirements.module';
import { AssignmentsModule } from '../assignments/assignments.module';
import {
  RequirementDataRepository
} from '../../database/repositories/requirements-data.repository';

@Module({
  imports: [
    UserModule,
    RequirementsModule,
    AssignmentsModule,
    RequirementsModule,
    TypeOrmModule.forFeature(
      [
        ProjectDataRepository,
        UserDataRepository,
        RequirementDataRepository
      ]
    )],
  providers: [ProjectsDataService, ProjectsService],
  controllers: [ProjectsController]
})
export class ProjectModule { }
