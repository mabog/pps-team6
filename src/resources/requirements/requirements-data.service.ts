import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  RequirementDataRepository
} from '../../database/repositories/requirements-data.repository';
import {
  RequirementReturnDTO
} from '../../models/requirements/requirement-return.dto';
import {
  ProjectDataRepository
} from '../../database/repositories/project-data.repository';
import {
  RequirementCreateDTO
} from '../../models/requirements/requirement-create.dto';
import { Project } from '../../database/entities/project.entity';
import { Requirement } from '../../database/entities/requirement.entity';
import { plainToClass } from 'class-transformer';
import { UserDisplayDTO } from '../../models/users/user-display.dto';
import { User } from '../../database/entities/user.entity';
import {
  UserDataRepository
} from '../../database/repositories/user-data.repository';
import {
  RequirementUpdateDTO
} from '../../models/requirements/requirement-update.dto';
import {
  ProjectSystemError
} from '../../middleware/exceptions/project-system.error';
import { Assignment } from '../../database/entities/assignment.entity';
import { Employee } from '../../database/entities/employee.entity';
import {
  AssignmentDataRepository
} from '../../database/repositories/assignment-data.repository';
import {
  EmployeeDataRepository
} from '../../database/repositories/employee-data.repository';

@Injectable()
export class RequirementsDataService {

  public constructor(
    @InjectRepository(RequirementDataRepository)
    private readonly reqDataRepo: RequirementDataRepository,
    @InjectRepository(ProjectDataRepository)
    private readonly projectDataRepo: ProjectDataRepository,
    @InjectRepository(UserDataRepository)
    private readonly userDataRepo: UserDataRepository,
    @InjectRepository(AssignmentDataRepository)
    private readonly assignmentsDataRepo: AssignmentDataRepository,
    @InjectRepository(EmployeeDataRepository)
    private readonly employeeDataRepo: EmployeeDataRepository,
  ) { }

  public async createRequirement(
    projectId: string,
    reqInfo: RequirementCreateDTO,
    user: UserDisplayDTO,
  ): Promise<RequirementReturnDTO> {
    const projectEntity: Project = await this
      .projectDataRepo
      .findProjectByID(projectId);

    const isSkillAlreadyRequired = (await projectEntity.requirements)
      .findIndex(
        project => project.reqSkill === reqInfo.reqSkill
      );

    if (isSkillAlreadyRequired > -1) {
      throw new ProjectSystemError(
        `Project already has ${reqInfo.reqSkill} as a requirement`, 409);
    }

    const userEntity: User = await this
      .userDataRepo
      .findUserEntityByEmail(user.email);

    const newRequirement: Requirement = await this
      .reqDataRepo
      .createRequirement(reqInfo, projectEntity, userEntity);

    return plainToClass(RequirementReturnDTO, newRequirement, {
      excludeExtraneousValues: true,
    })
  }


  public async updateRequirement(
    id: string,
    reqUpdateInfo?: Partial<RequirementUpdateDTO>,
  ): Promise<RequirementReturnDTO> {
    const reqEntity: Requirement = await this.reqDataRepo
      .updateRequirement(id, reqUpdateInfo);

    if (reqEntity.isCompleted) {
      const assignments: Assignment[] = await this.assignmentsDataRepo
        .completeAllRequirementAssignments(reqEntity.id);

      assignments.forEach(async assignment => {
        const employee: Employee = await this.employeeDataRepo
          .findEmployeeEntityById(assignment.assigneeId)

        this.employeeDataRepo.restoreAvailableWorkTime(
          employee,
          assignment.employeeWorkInputPerDay
        );
      });
    }

    return plainToClass(RequirementReturnDTO, reqEntity, {
      excludeExtraneousValues: true
    });
  }


  public async deleteRequirement(id: string): Promise<Requirement> {
    const reqEntity: Requirement = await this.reqDataRepo.findReqById(id);

    return await this.reqDataRepo.deleteRequirement(reqEntity);
  }
}
