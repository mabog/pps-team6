import { Test, TestingModule } from '@nestjs/testing';
import { RequirementsDataService } from '../requirements-data.service';

describe('RequirementsDataService', () => {
  let service: RequirementsDataService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RequirementsDataService],
    }).compile();

    service = module.get<RequirementsDataService>(RequirementsDataService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
