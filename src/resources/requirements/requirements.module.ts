import { Module } from '@nestjs/common';
import { RequirementsDataService } from './requirements-data.service';
import { RequirementsController } from './requirements.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  RequirementDataRepository
} from '../../database/repositories/requirements-data.repository';
import {
  ProjectDataRepository
} from '../../database/repositories/project-data.repository';
import {
  UserDataRepository
} from '../../database/repositories/user-data.repository';
import { AssignmentsModule } from '../assignments/assignments.module';
import { RequirementsService } from './requirements.service';
import {
  AssignmentDataRepository
} from '../../database/repositories/assignment-data.repository';
import {
  EmployeeDataRepository
} from '../../database/repositories/employee-data.repository';

@Module({
  imports:
    [
      AssignmentsModule,
      TypeOrmModule.forFeature(
        [
          RequirementDataRepository,
          AssignmentDataRepository,
          ProjectDataRepository,
          UserDataRepository,
          EmployeeDataRepository
        ]
      )],
  providers: [RequirementsDataService, RequirementsService],
  controllers: [RequirementsController],
  exports: [RequirementsDataService, RequirementsService]
})
export class RequirementsModule { }
