import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseUUIDPipe,
  UseGuards,
  Post,
  Body,
  ValidationPipe,
  Req,
  Patch,
  Delete
} from '@nestjs/common';
import {
  RequirementReturnDTO
} from '../../models/requirements/requirement-return.dto';
import { RequirementsDataService } from './requirements-data.service';
import {
  AuthGuardWithBlacklisting
} from '../../middleware/guards/blacklist.guard';
import {
  RequirementCreateDTO
} from '../../models/requirements/requirement-create.dto';
import { ResponseMessageDTO } from '../../models/common/response-message.dto';
import {
  RequirementUpdateDTO
} from '../../models/requirements/requirement-update.dto';
import { RequirementsService } from './requirements.service';
import { UserDisplayDTO } from '../../models/users/user-display.dto';

@Controller()
@UseGuards(AuthGuardWithBlacklisting)
export class RequirementsController {
  public constructor(
    private readonly requirementsDataService: RequirementsDataService,
    private readonly requirementsService: RequirementsService
  ) { }

  // Create req
  @Post('projects/:id/requirements')
  @HttpCode(HttpStatus.CREATED)
  public async createNewRequirement(
    @Param('id', ParseUUIDPipe) projectId: string,
    @Body(ValidationPipe) requirementInfo: RequirementCreateDTO,
    @Req() request,
  ): Promise<RequirementReturnDTO> {
    const user: UserDisplayDTO = request.user;

    return await this.requirementsDataService.createRequirement(
      projectId, requirementInfo, user
    );
  }


  // Update req
  @Patch('requirements/:id')
  @HttpCode(HttpStatus.OK)
  public async updateRequirement(
    @Param('id', ParseUUIDPipe) requirementId: string,
    @Body(ValidationPipe) reqUpdateInfo: Partial<RequirementUpdateDTO>
  ): Promise<RequirementReturnDTO> {

    return await this.requirementsDataService.updateRequirement(
      requirementId, reqUpdateInfo
    );
  }


  // Delete req
  @Delete('requirements/:id')
  @HttpCode(HttpStatus.OK)
  public async deleteRequirement(
    @Param('id', ParseUUIDPipe) reqId: string,
  ): Promise<ResponseMessageDTO> {
    await this.requirementsService.deleteRequirement(reqId);

    return { msg: 'Project requirement deleted' }
  }

}
