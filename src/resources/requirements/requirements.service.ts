import { Injectable } from '@nestjs/common';
import { RequirementsDataService } from './requirements-data.service';
import {
  AssignmentsDataService
} from '../assignments/assignments-data.service';
import { Requirement } from '../../database/entities/requirement.entity';
import {
  ProjectSystemError
} from '../../middleware/exceptions/project-system.error';

@Injectable()
export class RequirementsService {
  public constructor(
    private readonly assignmentsDataService: AssignmentsDataService,
    private readonly requirementsDataService: RequirementsDataService,
  ) { }

  public async deleteRequirement(reqId: string) {
    try {
      const reqEntity: Requirement = await this
        .requirementsDataService
        .deleteRequirement(reqId);

      if (reqEntity.isDeleted) {
        // Deletes all assignments and restores the working hours to employees
        const assignmentIds: string[] = (await reqEntity.assignments)
          .map(assignment => {
            if (!assignment.isDeleted) {

              return assignment.id;
            }
          });

        assignmentIds.forEach(
          id => this.assignmentsDataService.deleteAssignment(id)
        );
      }
    } catch (error) {
      throw new ProjectSystemError(error, 404);
    }
  }
}
