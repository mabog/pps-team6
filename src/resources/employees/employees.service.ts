import { Injectable } from '@nestjs/common';
import { EmployeeCreateDTO } from '../../models/employees/employee-create.dto';
import { EmployeeReturnDTO } from '../../models/employees/employee-return.dto';
import { Employee } from '../../database/entities/employee.entity';
import { plainToClass } from 'class-transformer';
import { EmployeesDataService } from './employees-data.service';
import {
  EmployeeSkillsUpdateDTO
} from '../../models/employees/employee-skills-update.dto';

@Injectable()
export class EmployeesService {
  public constructor(
    private readonly employeesDataService: EmployeesDataService,
  ) { }

  public async createEmployee(
    employeeInfo: EmployeeCreateDTO,
    skills: string,
  ): Promise<EmployeeReturnDTO> {
    const employeeEntity: Employee = await this.employeesDataService
      .createEmployee(
        employeeInfo,
        skills
      );

    return plainToClass(EmployeeReturnDTO, employeeEntity, {
      excludeExtraneousValues: true,
    });
  }


  public async addEmployeeSkills(
    employeeId: string,
    skillsUpdateInfo: Partial<EmployeeSkillsUpdateDTO>,
  ): Promise<EmployeeReturnDTO> {
    const employeeEntity: Employee = await this.employeesDataService
      .addEmployeeSkills(
        employeeId,
        skillsUpdateInfo
      );

    return plainToClass(EmployeeReturnDTO, employeeEntity, {
      excludeExtraneousValues: true,
    });
  }


  public async removeEmployeeSkill(
    employeeId: string,
    skillsUpdateInfo: Partial<EmployeeSkillsUpdateDTO>,
  ): Promise<EmployeeReturnDTO> {
    const employeeEntity: Employee = await this.employeesDataService
      .removeEmployeeSkill(
        employeeId,
        skillsUpdateInfo
      );

    return plainToClass(EmployeeReturnDTO, employeeEntity, {
      excludeExtraneousValues: true,
    });
  }

}
