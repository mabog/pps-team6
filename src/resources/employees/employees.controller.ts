import {
  Controller,
  Post,
  HttpCode,
  HttpStatus,
  UseGuards,
  Body,
  ValidationPipe,
  Get,
  ParseUUIDPipe,
  Param,
  Delete,
  Patch,
} from '@nestjs/common';
import { EmployeeCreateDTO } from '../../models/employees/employee-create.dto';
import { EmployeeReturnDTO } from '../../models/employees/employee-return.dto';
import { EmployeesDataService } from './employees-data.service';
import { AdminGuard } from '../../middleware/guards/admin.guard';
import {
  AuthGuardWithBlacklisting
} from '../../middleware/guards/blacklist.guard';
import { ResponseMessageDTO } from '../../models/common/response-message.dto';
import { EmployeeUpdateDTO } from '../../models/employees/employee-update.dto';
import { EmployeesService } from './employees.service';
import {
  EmployeeSkillsUpdateDTO
} from '../../models/employees/employee-skills-update.dto';

@Controller('employees')
@UseGuards(AuthGuardWithBlacklisting)
export class EmployeesController {
  public constructor(
    private readonly employeeDataService: EmployeesDataService,
    private readonly employeeService: EmployeesService,
  ) { }


  // Create employee
  @Post('/create')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AdminGuard)
  public async signUp(
    @Body(ValidationPipe) employeeInfo: EmployeeCreateDTO,
    @Body(ValidationPipe) skills: string,
  ): Promise<EmployeeReturnDTO> {

    return await this.employeeService.createEmployee(
      employeeInfo,
      skills['skills']
    );
  }


  // All employees
  @Get()
  @HttpCode(HttpStatus.OK)
  public async getAllUsers(): Promise<EmployeeReturnDTO[]> {

    return await this.employeeDataService.getAllEmployees();
  }


  // Single employee
  @Get('/:id')
  @HttpCode(HttpStatus.OK)
  public async getSingleEmployee(
    @Param('id', ParseUUIDPipe) employeeId: string,
  ): Promise<EmployeeReturnDTO> {

    return await this.employeeDataService.getSingleEmployee(employeeId);
  }


  // Update employee info
  @Patch('/:id')
  @HttpCode(HttpStatus.OK)
  public async updateEmployeeInfo(
    @Param('id', ParseUUIDPipe) employeeId: string,
    @Body() employeeUpdateInfo: Partial<EmployeeUpdateDTO>,
  ): Promise<EmployeeReturnDTO> {

    return this.employeeDataService.updateEmployeeInfo(
      employeeId,
      employeeUpdateInfo
    );
  }


  // Add employee skills
  @Patch('/:id/skills')
  @HttpCode(HttpStatus.OK)
  public async addEmployeeSkills(
    @Param('id', ParseUUIDPipe) employeeId: string,
    @Body() skillsUpdateInfo: Partial<EmployeeSkillsUpdateDTO>,
  ): Promise<EmployeeReturnDTO> {

    return await this.employeeService
      .addEmployeeSkills(
        employeeId,
        skillsUpdateInfo
      );
  }


  // Remove employee skill
  @Delete('/:id/skills')
  @HttpCode(HttpStatus.OK)
  public async removeEmployeeSkill(
    @Param('id', ParseUUIDPipe) employeeId: string,
    @Body() skillsUpdateInfo: Partial<EmployeeSkillsUpdateDTO>,
  ): Promise<EmployeeReturnDTO> {

    return await this.employeeService
      .removeEmployeeSkill(
        employeeId,
        skillsUpdateInfo
      );
  }


  // Delete employee
  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  public async deleteEmployee(
    @Param('id', ParseUUIDPipe) employeeId: string,
  ): Promise<ResponseMessageDTO> {
    await this.employeeDataService.deleteEmployee(employeeId);

    return { msg: 'Employee deleted' }
  }
}
