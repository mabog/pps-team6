import { Test, TestingModule } from '@nestjs/testing';
import { EmployeesDataService } from '../employees-data.service';

describe('EmployeeService', () => {
  let service: EmployeesDataService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EmployeesDataService],
    }).compile();

    service = module.get<EmployeesDataService>(EmployeesDataService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
