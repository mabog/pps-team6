import { Module } from '@nestjs/common';
import { EmployeesDataService } from './employees-data.service';
import { EmployeesController } from './employees.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  EmployeeDataRepository
} from '../../database/repositories/employee-data.repository';
import { EmployeesService } from './employees.service';
import {
  SkillDataRepository
} from '../../database/repositories/skill-data.repository';
import { UserModule } from '../users/users.module';
import { UserDataRepository } from '../../database/repositories/user-data.repository';

@Module({
  imports: [
    UserModule,
    TypeOrmModule.forFeature(
    [
      SkillDataRepository,
      EmployeeDataRepository,
      UserDataRepository
    ]
  )],
  providers: [EmployeesDataService, EmployeesService],
  controllers: [EmployeesController]
})
export class EmployeeModule { }
