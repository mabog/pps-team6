import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Skill } from '../../database/entities/skill.entity';
import { In } from 'typeorm';
import {
  EmployeeDataRepository
} from '../../database/repositories/employee-data.repository';
import { EmployeeCreateDTO } from '../../models/employees/employee-create.dto';
import { EmployeeReturnDTO } from '../../models/employees/employee-return.dto';
import { plainToClass } from 'class-transformer';
import { Employee } from '../../database/entities/employee.entity';
import { EmployeeUpdateDTO } from '../../models/employees/employee-update.dto';
import {
  SkillDataRepository
} from '../../database/repositories/skill-data.repository';
import {
  UserDataRepository
} from '../../database/repositories/user-data.repository';
import { User } from '../../database/entities/user.entity';
import {
  ProjectSystemError
} from '../../middleware/exceptions/project-system.error';
import {
  EmployeeSkillsUpdateDTO
} from '../../models/employees/employee-skills-update.dto';

@Injectable()
export class EmployeesDataService {

  public constructor(
    @InjectRepository(SkillDataRepository)
    private readonly skillsDataRepo: SkillDataRepository,
    @InjectRepository(EmployeeDataRepository)
    private readonly employeeDataRepo: EmployeeDataRepository,
    @InjectRepository(UserDataRepository)
    private readonly userDataRepo: UserDataRepository,
  ) { }

  public async getAllEmployees(): Promise<EmployeeReturnDTO[]> {
    const employees: Employee[] = await this.employeeDataRepo.find({
      isDeleted: false,
    });

    return plainToClass(EmployeeReturnDTO, employees, {
      excludeExtraneousValues: true,
    });
  }


  public async getSingleEmployee(id: string): Promise<EmployeeReturnDTO> {
    const employee: Employee = await this
      .employeeDataRepo
      .findEmployeeEntityById(id);

    return plainToClass(EmployeeReturnDTO, employee, {
      excludeExtraneousValues: true,
    });
  }


  public async createEmployee(
    employeeInfo: EmployeeCreateDTO,
    skills: string,
  ): Promise<Employee> {
    try {
      const employeeSkills: Skill[] = await this.skillsDataRepo.find({
        where: {
          skillName: In(skills.split(',')),
        }
      });

      const employeeEntity: Employee = await this.employeeDataRepo
        .createEmployee(
          employeeInfo, employeeSkills
        );

      if (employeeInfo.managerId) {
        const userEntity: User = await this.userDataRepo
          .findUserEntityById(employeeInfo.managerId);

        await this.employeeDataRepo.assignEmployeeToManager(
          userEntity, employeeEntity
        );
      }

      return employeeEntity;
    } catch (error) {
      if (error.errno === 1062) {
        throw new ProjectSystemError(
          'Employee with such e-mail already exists', 409
        );
      } else if (error.code === 404) {
        throw new ProjectSystemError(
          'Employee created without a manager. Manager does not exist', 404
        );
      }
    }
  }


  public async updateEmployeeInfo(
    id: string,
    employeeInfo: Partial<EmployeeUpdateDTO>
  ): Promise<EmployeeReturnDTO> {
    const foundEmployee: Employee = await this
      .employeeDataRepo
      .findEmployeeEntityById(id);

    const employeeToUpdate: Employee = { ...foundEmployee, ...employeeInfo };

    const savedEmployee: Employee = await this
      .employeeDataRepo
      .save(employeeToUpdate);

    return plainToClass(EmployeeReturnDTO, savedEmployee, {
      excludeExtraneousValues: true
    });
  }


  public async addEmployeeSkills(
    id: string,
    skillsUpdateInfo: Partial<EmployeeSkillsUpdateDTO>
  ): Promise<Employee> {
    if (!skillsUpdateInfo.newSkills) {
      throw new ProjectSystemError('newSkills field cannot be empty', 404);
    }

    const employeeEntity = await this.employeeDataRepo
      .findEmployeeEntityById(id);

    const skillsToAssign: Skill[] = await this.skillsDataRepo.find({
      where: {
        skillName: In(skillsUpdateInfo.newSkills.split(',')),
      }
    });

    return await this.employeeDataRepo.addEmployeeSkills(
      employeeEntity,
      skillsToAssign,
    );
  }


  public async removeEmployeeSkill(
    id: string,
    skillsUpdateInfo: Partial<EmployeeSkillsUpdateDTO>
  ): Promise<Employee> {
    const employeeEntity = await this.employeeDataRepo
      .findEmployeeEntityById(id);

    return await this.employeeDataRepo
      .removeEmployeeSkill(
        employeeEntity,
        skillsUpdateInfo.skillToRemove
      );
  }


  public async deleteEmployee(id: string): Promise<void> {
    const employeeEntity: Employee = await this
      .employeeDataRepo
      .findEmployeeEntityById(id);

    await this.employeeDataRepo.save({
      ...employeeEntity,
      isDeleted: true,
    });
  }
}
