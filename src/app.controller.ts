import { Controller, Get, Redirect, HttpStatus } from '@nestjs/common';

@Controller()
export class AppController {

  @Get()
  @Redirect('/session/login', HttpStatus.MOVED_PERMANENTLY)
  public root(): void {
    // redirects to /login
  }
}
