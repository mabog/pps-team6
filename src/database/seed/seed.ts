import { Repository, In, createConnection } from "typeorm";
import { Position } from "../entities/position.entity";
import { User } from "../entities/user.entity";
import { Skill } from "../entities/skill.entity";
import { UserPosition } from "../../models/users/enums/user-position.enum";
import { CompanySkills } from "../../models/skills/enums/skills.enum";
import { Employee } from "../entities/employee.entity";
import * as bcrypt from 'bcrypt';


const seedPositions = async (connection: any) => {
  const positionsRepo: Repository<Position> = connection
    .manager
    .getRepository(Position);

  const positions: Position[] = await positionsRepo.find();
  if (positions.length) {
    console.log('The DB already has positions!');
    return;
  }

  const rolesSeeding: Position[] = Object.keys(UserPosition).map(
    (positionName: string) => positionsRepo.create({ positionName })
  );

  await positionsRepo.save(rolesSeeding);
  console.log('Seeded positions successfully!');
};


const seedSkills = async (connection: any) => {
  const skillsRepo: Repository<Skill> = connection
    .manager
    .getRepository(Skill);

  const skills: Skill[] = await skillsRepo.find();
  if (skills.length) {
    console.log('The DB already has positions!');
    return;
  }

  const skillsSeeding: Skill[] = Object.keys(CompanySkills).map(
    (skillName: string) => skillsRepo.create({ skillName })
  );

  await skillsRepo.save(skillsSeeding);
  console.log('Seeded skills successfully!');
}


const seedAdmin = async (connection: any) => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);
  const positionsRepo: Repository<Position> = connection
    .manager
    .getRepository(Position);

  const admin = await userRepo.findOne({
    where: {
      email: 'admin@pps.com'
    },
  });

  if (admin) {
    console.log('The DB already has an admin!');
    return;
  }

  const positionNames: string[] = Object.keys(UserPosition);
  const allUserPositions: Position[] = await positionsRepo.find({
    where: {
      positionName: In(positionNames),
    },
  });

  if (allUserPositions.length === 0) {
    console.log('The DB does not have any positions!');
    return;
  }

  const firstName = 'admin';
  const lastName = 'adminov';
  const email = 'admin@pps.com';
  const salt = bcrypt.genSaltSync();
  const hashedPassword = await bcrypt.hash('Aaa123123', salt);

  const newAdmin: User = userRepo.create({
    firstName,
    lastName,
    email,
    password: hashedPassword,
    salt: salt,
    positions: allUserPositions,
    isAdmin: true,
    totalAvailableWorkTime: 8,
    isSelfManaged: true,
  });

  await userRepo.save(newAdmin);
  console.log('Seeded admin successfully!');
};


const seedUser = async (connection: any) => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);
  const positionsRepo: Repository<Position> = connection
    .manager
    .getRepository(Position);

  const user = await userRepo.find({
    where: {
      username: 'user',
    },
  });

  if (user.length > 1) {
    console.log('The DB already has a user!');
    return;
  }

  const positionNames: string[] = [];

  Object.keys(UserPosition).map(position => {
    if (position === 'REGULAR') {
      positionNames.push(position);
    }
  });

  const userPositions: Position[] = await positionsRepo.find({
    where: {
      positionName: In(positionNames)
    },
  });

  if (positionNames.length === 0) {
    console.log(('The DB does not have any roles!'));
    return;
  }

  const firstName = 'Michael';
  const lastName = 'Scott';
  const email = 'mgs@dundermifflin.com';
  const salt = bcrypt.genSaltSync();
  const hashedPassword = await bcrypt.hash('Test123!', salt);

  const newUser: User = userRepo.create({
    firstName,
    lastName,
    email,
    password: hashedPassword,
    salt: salt,
    positions: userPositions,
    totalAvailableWorkTime: 8,
    isSelfManaged: true
  });

  await userRepo.save(newUser);
  console.log('Seeded regular user successfully!');
}


const seedEmployee = async (connection: any) => {
  const employeeRepo: Repository<Employee> = connection
    .manager
    .getRepository(Employee);
  const skillsRepo: Repository<Skill> = connection.manager.getRepository(Skill);
  const skillNames: string[] = Object.keys(CompanySkills);

  const employee = await employeeRepo.find({
    where: {
      email: 'test'
    },
  });

  if (employee.length !== 0) {
    console.log('The DB already has a user!');
    return;
  }

  const allEmployeeSkills: Skill[] = await skillsRepo.find({
    where: {
      skillName: In(skillNames)
    },
  });

  if (allEmployeeSkills.length === 0) {
    console.log(('The DB does not have any skills!'));
    return;
  }

  const firstName = 'Pchelichko';
  const lastName = 'Pchelichkov';
  const email = 'bzz@pps.com';

  const newEmployee: Employee = employeeRepo.create({
    firstName,
    lastName,
    email,
    skills: allEmployeeSkills,
    totalAvailableWorkTime: 8,
    managedById: null,
  });

  await employeeRepo.save(newEmployee);
  console.log('Seeded employee successfully!');
}


const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

  await seedPositions(connection);
  await seedSkills(connection);
  await seedAdmin(connection);
  await seedUser(connection);
  await seedEmployee(connection);

  await connection.close();
  console.log('Seed completed!');
};

seed().catch(console.error);
