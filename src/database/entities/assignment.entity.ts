/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn
} from "typeorm";
import { Employee } from "./employee.entity";
import { Requirement } from "./requirement.entity";

@Entity('assignments')
export class Assignment {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({ type: 'nvarchar', nullable: false })
  public assignmentSkill: string;

  @Column({ type: 'int', nullable: false, default: 0 })
  public employeeWorkInputPerDay: number;

  @Column({ type: 'int', nullable: false, default: 0 })
  public totalTimeWorked: number;

  @Column({ type: 'uuid', nullable: false })
  public assigneeId: string;

  @Column({ type: 'uuid', nullable: false })
  public projectId: string;

  @Column({ type: 'uuid', nullable: false })
  public requirementId: string;

  @Column({ type: 'uuid', nullable: false })
  public assignerId: string;

  @Column({ type: 'bool', nullable: false, default: false })
  public isCompleted: boolean;

  @Column({ type: 'bool', nullable: false, default: false })
  public isDeleted: boolean;

  @CreateDateColumn({ type: 'datetime', nullable: false })
  public dateAssigned: string;

  @UpdateDateColumn({ type: 'datetime', nullable: false })
  public lastUpdated: string;

  @ManyToOne(type => Requirement, req => req.assignments, { lazy: true })
  public requirement: Promise<Requirement>;

  @ManyToOne(type => Employee, employee => employee.assignments, { lazy: true })
  public employee: Promise<Employee>
}