import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('positions')
export class Position {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ nullable: false })
  public positionName: string;
}
