/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Entity,
  ManyToOne,
  ManyToMany,
  JoinTable,
  OneToMany
} from "typeorm";
import { User } from "./user.entity";
import { Skill } from "./skill.entity";
import { Assignment } from "./assignment.entity";

@Entity('employees')
export class Employee {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({ type: 'nvarchar', nullable: false, length: 30 })
  public firstName: string;

  @Column({ type: 'nvarchar', nullable: false, length: 30 })
  public lastName: string;

  @Column({ type: 'nvarchar', nullable: false, unique: true })
  public email: string;

  @Column({ type: 'uuid', nullable: true, default: null })
  public managedById: string;

  @Column({ type: 'int', nullable: false, default: 8 })
  public totalAvailableWorkTime: number;

  @CreateDateColumn({ type: 'datetime', nullable: false })
  public dateCreated: string;

  @UpdateDateColumn({ type: 'datetime', nullable: false })
  public lastUpdated: string;

  @Column({ type: 'boolean', nullable: false, default: false })
  public isDeleted: boolean;

  @OneToMany(
    type => Assignment,
    assignment => assignment.employee,
    { lazy: true }
  )
  public assignments: Promise<Assignment[]>

  @ManyToOne(type => User, user => user.employees, { lazy: true })
  public user: Promise<User>

  @ManyToMany(type => Skill, { eager: true, cascade: true })
  @JoinTable()
  public skills: Skill[];
}
