import { ProjectStatus } from './../../models/projects/enum/project-status.enum';
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { User } from "./user.entity";
import { Requirement } from "./requirement.entity";

import * as moment from 'moment';

@Entity('projects')
export class Project {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({ type: 'nvarchar', nullable: false })
  public projectCreator: string;

  @Column({ type: 'uuid', nullable: false })
  public currentManagerId: string;

  @Column({ type: 'nvarchar', nullable: false, length: 80 })
  public name: string;

  @Column({ type: 'nvarchar', nullable: false, length: 3000 })
  public description: string;

  @Column({ type: 'nvarchar', nullable: false, default: ProjectStatus.IN_PROGRESS })
  public status: string;

  @Column({ type: 'int', nullable: false, default: 0 })
  public targetInDays: number;

  @Column({ type: 'int', nullable: false, default: 0 })
  public managerWorkInputPerDay: number;

  @Column({ type: 'int', nullable: false, default: 0 })
  public daysUntilCompletion: number;

  @Column(
    {
      type: 'datetime',
      nullable: false,
      default: moment(new Date().setFullYear(2050)).format('YYYY/MM/DD')
    })
  public estCompletionDate: string;

  @Column({ type: 'boolean', nullable: false, default: true })
  public isWithinTarget: boolean;

  @CreateDateColumn({ type: 'datetime', nullable: false })
  public dateCreated: string;

  @UpdateDateColumn({ type: 'datetime', nullable: false })
  public lastUpdated: string;

  @Column({ type: 'boolean', nullable: false, default: false })
  public isCompleted: boolean;

  @Column({ type: 'boolean', nullable: false, default: false })
  public isDeleted: boolean;

  @ManyToOne(type => User, user => user.projects, { lazy: true })
  public user: Promise<User>

  @OneToMany(type => Requirement, req => req.project, { lazy: true })
  public requirements: Promise<Requirement[]>
}
