/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany
} from "typeorm";
import { Project } from "./project.entity";
import { Assignment } from "./assignment.entity";
import { User } from "./user.entity";

@Entity('requirements')
export class Requirement {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({ type: 'nvarchar', nullable: false })
  public reqCreator: string;

  @Column({ type: 'nvarchar', nullable: false })
  public reqSkill: string;

  @Column({ type: 'int', nullable: false, default: 0 })
  public totalReqWorkTime: number;

  @Column({ type: 'int', nullable: false, default: 0 })
  public leftReqWorkTime: number;

  @Column({ type: 'int', nullable: false, default: 0 })
  public totalCompletedWorkTime: number;

  @Column({ type: 'int', nullable: false, default: 0 })
  public completedWorkTimePerDay: number;

  @Column({ type: 'int', nullable: false, default: 0 })
  public daysUntilCompletion: number;

  @Column({ type: 'datetime', nullable: false, default: undefined })
  public estCompletionDate: string;

  @Column({ type: 'boolean', nullable: false, default: false })
  public isCompleted: boolean;

  @CreateDateColumn({ type: 'datetime', nullable: false })
  public dateCreated: string;

  @UpdateDateColumn({ type: 'datetime', nullable: false })
  public lastUpdated: string;

  @Column({ type: 'boolean', nullable: false, default: false })
  public isDeleted: boolean;

  @ManyToOne(type => Project, project => project.requirements, { lazy: true })
  public project: Promise<Project>

  @ManyToOne(type => User, user => user.requirements, { lazy: true })
  public user: Promise<User>

  @OneToMany(
    type => Assignment,
    assignment => assignment.requirement,
    { lazy: true }
  )
  public assignments: Assignment[];
}
