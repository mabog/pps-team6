import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('skills')
export class Skill {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ type: 'nvarchar', nullable: false })
  public skillName: string;
}
