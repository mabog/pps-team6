/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Entity,
  OneToMany,
  ManyToMany,
  JoinTable,
  ManyToOne
} from "typeorm";
import { Employee } from "./employee.entity";
import { Project } from "./project.entity";
import { Position } from "./position.entity";
import { Requirement } from "./requirement.entity";
import { Skill } from "./skill.entity";

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({ type: 'nvarchar', nullable: false, length: 30 })
  public firstName: string;

  @Column({ type: 'nvarchar', nullable: false, length: 30 })
  public lastName: string;

  @Column({ type: 'nvarchar', nullable: false, unique: true })
  public email: string;

  @Column({ type: 'nvarchar', nullable: false })
  public password: string;

  @Column({ type: 'nvarchar', nullable: false })
  public salt: string;

  @Column({ type: 'int', nullable: false })
  public totalAvailableWorkTime: number;

  @Column({ type: 'boolean', nullable: false, default: false })
  public isAdmin: boolean;

  @Column({ type: 'boolean', nullable: false, default: true })
  public isSelfManaged: boolean;

  @Column({ type: 'uuid', nullable: true, default: null })
  public managerId: string;

  @CreateDateColumn({ type: 'datetime', nullable: false })
  public dateCreated: string;

  @UpdateDateColumn({ type: 'datetime', nullable: false })
  public lastUpdated: string;

  @Column({ type: 'boolean', nullable: false, default: false })
  public isDeleted: boolean;

  @Column({ type: 'boolean', nullable: false, default: true })
  public hasAccess: boolean;

  @Column({ type: 'nvarchar', default: 'User has access' })
  public hasNoAccessReason: string;

  @Column({ nullable: false, default: '' })
  public avatarUrl: string;

  @OneToMany(type => Employee, employee => employee.user, { lazy: true })
  public employees: Promise<Employee[]>

  @OneToMany(type => Project, project => project.user, { lazy: true })
  public projects: Promise<Project[]>

  @OneToMany(type => Requirement, req => req.user, { lazy: true })
  public requirements: Promise<Requirement[]>

  @ManyToMany(type => Position, { eager: true })
  @JoinTable()
  public positions: Position[];

  @ManyToMany(type => Skill, { eager: true })
  @JoinTable()
  public skills: Skill[];

  // Self-reference relationship
  @ManyToOne(type => User, user => user.managing)
  public managedBy: User;

  @OneToMany(type => User, user => user.managedBy)
  public managing: User[];
}