import { Repository, EntityRepository, getConnection } from "typeorm";
import { Assignment } from "../entities/assignment.entity";
import { Employee } from "../entities/employee.entity";
import {
  ProjectSystemError
} from "../../middleware/exceptions/project-system.error";
import { Requirement } from "../entities/requirement.entity";
import { User } from "../entities/user.entity";
import {
  AssignmentUpdateDTO
} from "../../models/assignments/assignment-update.dto";
import {
  AssignmentCreateDTO
} from "../../models/assignments/assignment-create.dto";

import * as moment from 'moment';


@EntityRepository(Assignment)
export class AssignmentDataRepository extends Repository<Assignment> {
  public async getAllEmployeeAssignments(
    employeeEntity: Employee,
  ): Promise<Assignment[]> {
    await this.updateAllAssignmentsTotalTimeWorked();

    const assignments: Assignment[] = await this.find({
      where: {
        assigneeId: employeeEntity.id,
        isDeleted: false
      }
    });

    if (assignments.length === 0) {
      throw new ProjectSystemError('Employee has no assignments', 404);
    }

    return assignments;
  }


  public async getAllRequirementAssignments(
    reqEntity: Requirement,
  ): Promise<Assignment[]> {
    await this.updateAllAssignmentsTotalTimeWorked();

    const assignments: Assignment[] = await this.find({
      where: {
        requirementId: reqEntity.id,
        isDeleted: false
      }
    });

    if (assignments.length === 0) {
      throw new ProjectSystemError(
        'Project Requirement has no assignments', 404
      );
    }

    return assignments;
  }


  public async createAssignment(
    employeeEntity: Employee,
    userEntity: User,
    reqEntity: Requirement,
    employeeWorkInputPerDay: number,
  ): Promise<Assignment> {

    const assignmentEntity: Assignment = this.create(
      {
        assignmentSkill: reqEntity.reqSkill,
        employeeWorkInputPerDay,
        projectId: (await reqEntity.project).id,
        assigneeId: employeeEntity.id,
        assignerId: userEntity.id,
        requirementId: reqEntity.id
      }
    );

    assignmentEntity.requirement = Promise.resolve(reqEntity);
    assignmentEntity.employee = Promise.resolve(employeeEntity);

    return await this.save(assignmentEntity);
  }


  public async updateAssignment(
    assignmentEntity: Assignment,
    employeeEntity: Employee,
    updateInfo: Partial<AssignmentUpdateDTO>
  ): Promise<Assignment> {

    assignmentEntity.employee = Promise.resolve(employeeEntity);

    const updatedAssignment: Assignment = {
      ...assignmentEntity,
      assigneeId: employeeEntity.id,
      ...updateInfo
    };

    return await this.save(updatedAssignment);
  }


  public async deleteAssignment(
    assignmentEntity: Assignment
  ): Promise<Assignment> {
    assignmentEntity.isDeleted = true;

    return await this.save(assignmentEntity)
  }


  public async completeAllRequirementAssignments(
    requirementId: string
  ) {
    const assignments: Assignment[] = await this.find({
      where: {
        requirementId
      }
    });

    assignments.forEach(async assignment => {
      assignment.isCompleted = true;

      return await this.save(assignment);
    });

    return assignments;
  }


  public async getTotalWorkInputPerDayForReq(
    requirementId: string
  ): Promise<number> {
    const assignments: Assignment[] = await this.find({
      where: {
        requirementId
      }
    });

    return assignments.reduce(
      (acc, assignment) => acc += assignment.employeeWorkInputPerDay, 0
    );
  }


  public async getTotalCompletedWorkForReq(
    requirementId: string
  ): Promise<number> {
    const assignments: Assignment[] = await this.find({
      where: {
        requirementId
      }
    });

    return assignments.reduce(
      (acc, assignment) => acc += assignment.totalTimeWorked, 0
    );
  }


  public async updateTotalTimeWorked(assignmentEntity: Assignment) {
    const daysPassed = this.calculateDaysPassed(
      assignmentEntity.lastUpdated
    );
    assignmentEntity.totalTimeWorked +=
      assignmentEntity.employeeWorkInputPerDay * daysPassed;

    if (daysPassed === 0) {

      return;
    }

    // Query Builder is mandatory here, because it resolves a major issue
    // where with this.save() did not save the entity on time
    return await getConnection()
      .createQueryBuilder()
      .update(Assignment)
      .set({ totalTimeWorked: assignmentEntity.totalTimeWorked })
      .where("id = :id", { id: assignmentEntity.id })
      .execute();
  }


  public calculateDaysPassed(lastUpdatedDate: string): number {
    const lastDate = moment(lastUpdatedDate);

    // Weekends are not working days
    if (lastDate.day() === 6 || lastDate.day() === 0) {

      return 0;
    }

    const currentDate = moment(new Date());
    const timePassed = moment.duration(currentDate.diff(lastDate));
    const daysPassed = Math.round(timePassed.asDays());

    if (daysPassed < 0) {
      throw new ProjectSystemError(
        `lastUpdatedDate ${moment(lastUpdatedDate).format('DD-MM-YYYY')}\
 is in the future`, 409
      );
    }

    return daysPassed;
  }


  public async isEmployeeAlreadyAssignedToRequirement(
    employeeEntity: Employee,
    assignmentInfo: AssignmentCreateDTO,
  ): Promise<boolean> {
    const assignments = await this.find({
      where: {
        isDeleted: false,
      }
    });

    const isAssigned: boolean[] = assignments.map(assignment => {
      if (
        assignment.assigneeId === employeeEntity.id &&
        assignment.requirementId === assignmentInfo.reqId
      ) {

        return true;
      }
    });

    if (isAssigned.includes(true)) {
      throw new ProjectSystemError(
        'Employee is already assigned to this requirement. \
Please use Update Assignment endpoint instead', 409
      );
    }

    return false;
  }


  public async findAssignmentEntityById(id: string): Promise<Assignment> {
    const assignmentEntity: Assignment = await this.findOne(id);

    if (!assignmentEntity) {
      throw new ProjectSystemError('Assignment does not exist', 404);
    }

    return assignmentEntity;
  }


  private async updateAllAssignmentsTotalTimeWorked(): Promise<void> {
    // Sort the assignments by lastUpdated for optimization! 
    const allAssignments: Assignment[] = await this.find({
      where: {
        isDeleted: false,
        isCompleted: false
      }
    });

    allAssignments.map(
      async assignment => await this.updateTotalTimeWorked(assignment)
    );
  }
}
