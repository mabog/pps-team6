import { Repository, EntityRepository } from "typeorm";
import { Employee } from "../entities/employee.entity";
import { Skill } from "../entities/skill.entity";
import { EmployeeCreateDTO } from "../../models/employees/employee-create.dto";
import {
  ProjectSystemError
} from "../../middleware/exceptions/project-system.error";
import { Requirement } from "../entities/requirement.entity";
import { User } from "../entities/user.entity";

@EntityRepository(Employee)
export class EmployeeDataRepository extends Repository<Employee> {

  public async createEmployee(
    employeeInfo: EmployeeCreateDTO,
    employeeSkills: Skill[],
  ): Promise<Employee> {
    const employeeEntity: Employee = this.create({
      ...employeeInfo,
      skills: employeeSkills,
      assignments: Promise.resolve([]),
    });

    return await this.save(employeeEntity);
  }


  public async addEmployeeSkills(
    employeeEntity: Employee,
    employeeNewSkills: Skill[],
  ): Promise<Employee> {
    employeeEntity.skills = employeeNewSkills;

    return await this.save(employeeEntity);
  }


  public async removeEmployeeSkill(
    employeeEntity: Employee,
    skillToRemove: string
  ): Promise<Employee> {
    employeeEntity.skills = employeeEntity.skills.filter(employeeSkill => {

      return employeeSkill.skillName !== skillToRemove;
    });

    return await this.save(employeeEntity);
  }


  public canEmployeeWorkAssignment(
    employeeEntity: Employee,
    hours: number,
    reqEntity?: Requirement,
  ): boolean {

    if (reqEntity) {
      const hasRequiredSkill: Skill = employeeEntity.skills.find(
        skill => skill.skillName === reqEntity.reqSkill
      );

      if (!hasRequiredSkill) {
        throw new ProjectSystemError(
          'Employee does not have the required skills for this assignment', 409
        );
      }
    }

    if (hours > employeeEntity.totalAvailableWorkTime) {
      throw new ProjectSystemError(
        'Employee does not have the required work hours for this assignment', 409
      );
    }

    return true;
  }


  public async assignEmployeeToManager(
    userEntity: User,
    employeeEntity: Employee,
  ): Promise<Employee> {
    employeeEntity.user = Promise.resolve(userEntity);
    employeeEntity.managedById = userEntity.id;

    return await this.save(employeeEntity);
  }


  public async reassignWork(
    oldEmployeeId: string,
    newEmployee: Employee,
    employeeWorkInputPerDay: number
  ): Promise<Employee> {
    this.canEmployeeWorkAssignment(newEmployee, employeeWorkInputPerDay);

    const oldEmployee: Employee = await this
      .findEmployeeEntityById(oldEmployeeId);

    await this.restoreAvailableWorkTime(
      oldEmployee,
      employeeWorkInputPerDay
    );

    return await this.deductAvailableWorkTime(
      newEmployee,
      employeeWorkInputPerDay
    );
  }


  public async updateWorkInput(
    employeeEntity: Employee,
    currentWorkInput: number,
    newWorkInput: number,
  ) {
    const timeDiff: number = currentWorkInput - newWorkInput;

    if (timeDiff < 0) {

      return this.deductAvailableWorkTime(employeeEntity, Math.abs(timeDiff));
    } else {

      return this.restoreAvailableWorkTime(employeeEntity, timeDiff);
    }
  }


  public async deductAvailableWorkTime(
    employeeEntity: Employee,
    hours: number
  ): Promise<Employee> {
    if (employeeEntity.totalAvailableWorkTime - Number(hours) < 0) {
      throw new ProjectSystemError(
        'Employee does not have the required work hours', 409
      );
    }

    employeeEntity.totalAvailableWorkTime -= Number(hours);

    return await this.save(employeeEntity);
  }


  public async restoreAvailableWorkTime(
    employeeEntity: Employee,
    hours: number
  ) {
    if (employeeEntity.totalAvailableWorkTime + Number(hours) > 8) {
      throw new ProjectSystemError(
        'Employee cannot have more than 8 hours totalAvailableWorkTime', 409
      );
    }
    employeeEntity.totalAvailableWorkTime += Number(hours);

    return await this.save(employeeEntity);
  }


  public async findEmployeeEntityByEmail(email: string): Promise<Employee> {
    const employeeEntity: Employee = await this.findOne({
      where: {
        email,
        isDeleted: false,
      }
    });

    if (!employeeEntity) {
      throw new ProjectSystemError('Employee does not exist', 404);
    }

    return employeeEntity;
  }


  public async findEmployeeEntityById(id: string): Promise<Employee> {
    const employeeEntity: Employee = await this.findOne({
      where: {
        id,
        isDeleted: false,
      }
    });

    if (!employeeEntity) {
      throw new ProjectSystemError('Employee does not exist', 404);
    }

    return employeeEntity;
  }

}
