import { Repository, EntityRepository } from "typeorm";
import { User } from "../entities/user.entity";
import { Position } from "../entities/position.entity";
import { Skill } from "../entities/skill.entity";
import { UserCreateDTO } from "../../models/users/user-create.dto";
import {
  ProjectSystemError
} from "../../middleware/exceptions/project-system.error";
import { UserDisplayDTO } from "../../models/users/user-display.dto";
import { plainToClass } from "class-transformer";
import { UserLoginDTO } from "../../models/users/user-login.dto";

import * as bcrypt from 'bcrypt';


@EntityRepository(User)
export class UserDataRepository extends Repository<User> {
  public async createUser(
    userInfo: UserCreateDTO,
    positionsToAssign: Position[],
    userSkills: Skill[]
  ): Promise<User> {
    const salt: string = await bcrypt.genSalt();
    userInfo.password = await this.hashPassword(userInfo.password, salt);

    const userEntity = this.create({
      ...userInfo,
      salt,
      skills: userSkills,
      positions: positionsToAssign,
      projects: Promise.resolve([])
    });

    return await this.save(userEntity);
  }


  public async assignUserToManager(
    userEntity: User,
    managerEntity: User
  ): Promise<User> {
    userEntity.managedBy = await Promise.resolve(managerEntity);
    userEntity.managerId = managerEntity.id;
    userEntity.isSelfManaged = false;

    return await this.save(userEntity);
  }


  public canUserWorkProject(
    userEntity: User,
    hours: number,
  ): boolean {
    if (hours > userEntity.totalAvailableWorkTime) {
      throw new ProjectSystemError(
        'User does not have the required work hours for this project', 409
      );
    }

    return true;
  }


  public async recalculateWorkHours(
    oldUserId: string,
    newUser: User,
    userWorkInputPerDay: number
  ): Promise<User> {
    this.canUserWorkProject(newUser, userWorkInputPerDay);

    const oldUser: User = await this
      .findUserEntityById(oldUserId);

    await this.restoreAvailableWorkTime(
      oldUser,
      userWorkInputPerDay
    );

    return await this.deductAvailableWorkTime(
      newUser,
      userWorkInputPerDay
    );
  }


  public async updateWorkInput(
    userId: string,
    currentWorkInput: number,
    newWorkInput: number,
  ) {
    const userEntity: User = await this.findUserEntityById(userId);

    const timeDiff: number = currentWorkInput - newWorkInput;

    if (timeDiff < 0) {

      return this.deductAvailableWorkTime(userEntity, Math.abs(timeDiff));
    } else {

      return this.restoreAvailableWorkTime(userEntity, timeDiff);
    }
  }



  public async deductAvailableWorkTime(
    userEntity: User,
    hours: number
  ): Promise<User> {
    if (userEntity.totalAvailableWorkTime - Number(hours) < 0) {
      throw new ProjectSystemError(
        'User does not have the required work hours', 409
      );
    }

    userEntity.totalAvailableWorkTime -= Number(hours);

    return await this.save(userEntity);
  }


  public async restoreAvailableWorkTime(
    userEntity: User,
    hours: number
  ) {
    if (userEntity.totalAvailableWorkTime + Number(hours) > 8) {
      throw new ProjectSystemError(
        'User cannot have more than 8 hours totalAvailableWorkTime', 409
      );
    }
    userEntity.totalAvailableWorkTime += Number(hours);

    return await this.save(userEntity);
  }


  public async findUserEntityByEmail(email: string): Promise<User> {
    const userEntity: User = await this.findOne({
      where: {
        email,
        isDeleted: false,
      }
    });

    if (!userEntity) {
      throw new ProjectSystemError('User does not exist', 404);
    }

    return userEntity;
  }


  public async findUserEntityById(id: string): Promise<User> {
    const userEntity: User = await this.findOne({
      where: {
        id,
        isDeleted: false,
      }
    });

    if (!userEntity) {
      throw new ProjectSystemError('User does not exist', 404);
    }

    return userEntity;
  }


  public async findUserDisplayDTOByEmail(
    email: string
  ): Promise<UserDisplayDTO> {
    const foundUser: User = await this.findOne({
      where: {
        email,
        isDeleted: false
      }
    });

    if (!foundUser) {
      throw new ProjectSystemError('User does not exist', 404);
    }

    return plainToClass(UserDisplayDTO, foundUser, {
      excludeExtraneousValues: true,
    });
  }


  public async validateUserPassword(user: UserLoginDTO): Promise<boolean> {
    const userEntity: User = await this.findOne({
      email: user.email,
    });

    return await bcrypt.compare(user.password, userEntity.password);
  }


  private async hashPassword(password: string, salt: string): Promise<string> {
    return bcrypt.hash(password, salt);
  }
}