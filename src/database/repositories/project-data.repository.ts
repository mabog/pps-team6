import { User } from './../entities/user.entity';
import { Repository, EntityRepository } from "typeorm";
import { Project } from "../entities/project.entity";
import { ProjectCreateDTO } from "../../models/projects/project-create.dto";
import {
  ProjectSystemError
} from '../../middleware/exceptions/project-system.error';
import { ProjectUpdateDTO } from '../../models/projects/project-update.dto';

@EntityRepository(Project)
export class ProjectDataRepository extends Repository<Project> {

  public async createProject(
    projectInfo: ProjectCreateDTO,
    projectManager: User
  ): Promise<Project> {
    const projectEntity: Project = this.create({ ...projectInfo });
    projectEntity.user = Promise.resolve(projectManager);
    projectEntity.projectCreator = projectManager.id;
    projectEntity.currentManagerId = projectManager.id;
    projectEntity.daysUntilCompletion = projectEntity.targetInDays;
    return await this.save(projectEntity);
  }


  public async deleteProject(
    projectEntity: Project
  ): Promise<Project> {
    projectEntity.isDeleted = true;

    return await this.save(projectEntity);
  }


  public async updateProject(
    projectEntity: Project,
    updateInfo: Partial<ProjectUpdateDTO>
  ): Promise<Project> {
    const updatedProject: Project = { ...projectEntity, ...updateInfo };

    return await this.save(updatedProject);
  }


  public async reassignProject(
    projectEntity: Project,
    newUserEntity: User
  ): Promise<Project> {
    projectEntity.user = Promise.resolve(newUserEntity);
    projectEntity.currentManagerId = newUserEntity.id;

    return await this.save(projectEntity);
  }


  public async completeProject(
    projectEntity: Project,
  ): Promise<Project> {
    projectEntity.isCompleted = true;

    return await this.save(projectEntity);
  }


  public async findProjectByID(projectID: string): Promise<Project> {
    const projectEntity: Project = await this.findOne(
      {
        where: {
          isDeleted: false,
          user: projectID
        }
    });

    if (!projectEntity) {
      throw new ProjectSystemError(`Project does not exist`, 404);
    }

    return projectEntity;
  }


  public async findProjectsByManager(userId: string): Promise<Project[]> {
    const projectEntity: Project[] = await this.find(
      {
        where: {
          isDeleted: false,
          user: userId
        }
      });

    if (projectEntity.length === 0) {
      throw new ProjectSystemError('Manager has no Projects', 404);
    }

    return projectEntity;
  }
}
