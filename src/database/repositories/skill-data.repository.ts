import { Repository, EntityRepository } from "typeorm";
import { Skill } from "../entities/skill.entity";
import { SkillCreateDTO } from "../../models/skills/skill-create.dto";
import {
  ProjectSystemError
} from "../../middleware/exceptions/project-system.error";

@EntityRepository(Skill)
export class SkillDataRepository extends Repository<Skill> {
  public async addNewSkill(newSkill: SkillCreateDTO) {
    const { skillName } = newSkill;

    const skills: Skill[] = await this.find({
      skillName: skillName.toUpperCase()
    });

    if (skills.length !== 0) {
      throw new ProjectSystemError('Skill already exists in DB', 409)
    }

    const skillEntity = this.create({
      skillName: skillName.toUpperCase()
    });

    return await this.save({
      ...skillEntity
    });
  }
}
