import { EntityRepository, Repository } from "typeorm";
import { Requirement } from "../entities/requirement.entity";
import { Project } from "../entities/project.entity";
import { User } from "../entities/user.entity";
import {
  RequirementCreateDTO
} from "../../models/requirements/requirement-create.dto";
import {
  RequirementUpdateDTO
} from "../../models/requirements/requirement-update.dto";
import {
  ProjectSystemError
} from "../../middleware/exceptions/project-system.error";
import {
  AssignmentReturnDTO
} from "../../models/assignments/assignment-return.dto";
import * as moment from 'moment';
import {
  RequirementLongestCompletionDateDTO
} from "../../models/requirements/requirement-longest-completion-date.dto";


@EntityRepository(Requirement)
export class RequirementDataRepository extends Repository<Requirement> {

  public async createRequirement(
    reqInfo: RequirementCreateDTO,
    projectEntity: Project,
    userEntity: User,
  ): Promise<Requirement> {

    const reqEntity: Requirement = this.create({ ...reqInfo });

    reqEntity.project = Promise.resolve(projectEntity);
    reqEntity.user = Promise.resolve(userEntity);
    reqEntity.reqCreator = await Promise.resolve(userEntity.id);
    reqEntity.estCompletionDate = new Date().toDateString();
    reqEntity.leftReqWorkTime = reqInfo.totalReqWorkTime;

    return await this.save(reqEntity);
  }


  public async updateRequirement(
    id: string,
    reqUpdateInfo?: Partial<RequirementUpdateDTO>
  ): Promise<Requirement> {
    const reqEntity: Requirement = await this.findReqById(id);
    const updatedInfo = this.validateReqUpdateInfo(reqUpdateInfo, reqEntity);
    const updatedWorkTime = updatedInfo.totalCompletedWorkTime;
    const totalReqWorkTime = updatedInfo.totalReqWorkTime;

    // If the total needed hours for the requirement are changed,
    // checks if work has already been invested, so it can update accordingly
    if (reqEntity.totalCompletedWorkTime === 0 && updatedWorkTime === 0) {
      reqEntity.leftReqWorkTime = totalReqWorkTime;
    } else {
      reqEntity.totalCompletedWorkTime +=
        updatedWorkTime - reqEntity.totalCompletedWorkTime;

      reqEntity.leftReqWorkTime =
        totalReqWorkTime - reqEntity.totalCompletedWorkTime;
    }

    const updatedReq: Requirement = { ...reqEntity, ...reqUpdateInfo };

    if (reqUpdateInfo.completedWorkTimePerDay) {
      updatedReq.completedWorkTimePerDay = reqUpdateInfo.completedWorkTimePerDay;

      await this.updateDaysUntilCompletion(updatedReq);
    }

    if (updatedReq.leftReqWorkTime <= 0) {
      await this.completeRequirement(updatedReq);
    }

    return await this.save(updatedReq);
  }


  public async deleteRequirement(reqEntity: Requirement): Promise<Requirement> {
    reqEntity.isDeleted = true;

    return await this.save(reqEntity);
  }


  public async updateCompletedWorkTimePerDay(
    reqEntity: Requirement,
    assignmentEntity: AssignmentReturnDTO,
  ): Promise<Requirement> {
    const { completedWorkTimePerDay } = reqEntity;
    const { employeeWorkInputPerDay } = assignmentEntity;

    const timeDiff: number = completedWorkTimePerDay - employeeWorkInputPerDay;

    if (timeDiff < 0) {

      return this.addCompletedWorkTimePerDay(reqEntity, Math.abs(timeDiff));
    } else {

      return this.deductCompletedWorkTimePerDay(reqEntity, timeDiff);
    }
  }


  public async addCompletedWorkTimePerDay(
    reqEntity: Requirement,
    workInputPerDay: number
  ): Promise<Requirement> {
    reqEntity.completedWorkTimePerDay += Number(workInputPerDay);

    return await this.updateDaysUntilCompletion(reqEntity);
  }


  public async deductCompletedWorkTimePerDay(
    reqEntity: Requirement,
    workInputPerDay: number
  ): Promise<Requirement> {
    reqEntity.completedWorkTimePerDay -= Number(workInputPerDay);

    return await this.updateDaysUntilCompletion(reqEntity);
  }


  public async findReqById(id: string): Promise<Requirement> {
    const reqEntity: Requirement = await this.findOne({
      where: {
        id,
        isDeleted: false,
        isCompleted: false
      }
    });

    if (!reqEntity) {
      throw new ProjectSystemError(`Requirement does not exist`, 404);
    }

    return reqEntity;
  }


  public async getRequirementsLongestCompletionDayAndDate(
    projectId: string
  ) {
    const requirements: Requirement[] = await this.find({
      where: {
        project: projectId,
      }
    });

    if (requirements.length === 0) {

      return;
    }
    
    const longestDayAndDate = new RequirementLongestCompletionDateDTO();

    requirements.sort((a, b) => b.daysUntilCompletion - a.daysUntilCompletion);

    longestDayAndDate.daysUntilCompletion = requirements[0].daysUntilCompletion;
    longestDayAndDate.estCompletionDate = requirements[0].estCompletionDate;

    return longestDayAndDate;
  }


  private async updateDaysUntilCompletion(
    reqEntity: Requirement
  ): Promise<Requirement> {
    const {
      totalReqWorkTime,
      totalCompletedWorkTime,
      completedWorkTimePerDay,
    } = reqEntity;

    if (reqEntity.completedWorkTimePerDay !== 0) {
      reqEntity.daysUntilCompletion =
        (totalReqWorkTime - totalCompletedWorkTime) / completedWorkTimePerDay;

      // Weekends are not working days
      reqEntity.daysUntilCompletion += (Math.floor(Math.floor(
        reqEntity.daysUntilCompletion / 5) * 2)
      );
    } else {
      reqEntity.daysUntilCompletion = 0;
    }

    return this.updateEstimatedCompletionDate(reqEntity);
  }

  private async updateEstimatedCompletionDate(
    reqEntity: Requirement,
  ): Promise<Requirement> {
    const { daysUntilCompletion, lastUpdated } = reqEntity;

    reqEntity.estCompletionDate = moment(lastUpdated)
      .add(daysUntilCompletion, 'day')
      .toLocaleString();

    return await this.save(reqEntity);
  }

  private async completeRequirement(
    reqEntity: Requirement
  ): Promise<Requirement> {
    reqEntity.isCompleted = true;
    await this.updateDaysUntilCompletion(reqEntity);

    return this.save(reqEntity);
  }

  private validateReqUpdateInfo(
    reqUpdateInfo: Partial<RequirementUpdateDTO>,
    reqEntity: Requirement,
  ): Partial<RequirementUpdateDTO> {
    if (!reqUpdateInfo.totalCompletedWorkTime) {
      reqUpdateInfo.totalCompletedWorkTime = reqEntity.totalCompletedWorkTime;
    } else {
      reqUpdateInfo.totalCompletedWorkTime = reqUpdateInfo.totalCompletedWorkTime;
    }

    if (!reqUpdateInfo.totalReqWorkTime) {
      reqUpdateInfo.totalReqWorkTime = reqEntity.totalReqWorkTime;
    } else {
      reqUpdateInfo.totalReqWorkTime = reqUpdateInfo.totalReqWorkTime;
    }

    return reqUpdateInfo;
  }

}
