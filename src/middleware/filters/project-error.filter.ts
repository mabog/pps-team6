import { ExceptionFilter, Catch, ArgumentsHost } from "@nestjs/common";
import { ProjectSystemError } from "../exceptions/project-system.error";
import { Response } from 'express';

@Catch(ProjectSystemError)
export class ProjectSystemErrorFilter implements ExceptionFilter {
  public catch(exception: ProjectSystemError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const res = ctx.getResponse<Response>();

    res.status(exception.code).json({
      status: exception.code,
      error: exception.message,
    });
  }
}
