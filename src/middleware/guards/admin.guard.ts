import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import { UserPosition } from "../../models/users/enums/user-position.enum";


@Injectable()
export class AdminGuard implements CanActivate {
  public canActivate(context: ExecutionContext): boolean {
    const req = context.switchToHttp().getRequest();
    const user = req.user;

    return user && user.positions.includes(UserPosition.ADMIN);
  }
}
