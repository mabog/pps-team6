import {
  Controller,
  Post,
  HttpCode,
  HttpStatus,
  Body,
  Delete,
  UseGuards
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserLoginDTO } from '../models/users/user-login.dto';
import { ResponseMessageDTO } from '../models/common/response-message.dto';
import { AuthGuardWithBlacklisting } from '../middleware/guards/blacklist.guard';
import { Token } from '../middleware/decorators/token.decorator';

@Controller('session')
export class AuthController {
  public constructor(
    private readonly authService: AuthService) { }


  @Post('login')
  @HttpCode(HttpStatus.OK)
  public async login(@Body() user: UserLoginDTO
  ): Promise<{ accessToken: string }> {

    return await this.authService.logIn(user);
  }


  @Delete()
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuardWithBlacklisting)
  public async logoutUser(@Token() token: string): Promise<ResponseMessageDTO> {
    this.authService.blacklistToken(token);

    return {
      msg: 'Successful logout!',
    };
  }

}

