import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserLoginDTO } from '../models/users/user-login.dto';
import { UserDisplayDTO } from '../models/users/user-display.dto';
import {
  ProjectSystemError
} from '../middleware/exceptions/project-system.error';
import {
  UserDataRepository
} from '../database/repositories/user-data.repository';

@Injectable()
export class AuthService {
  private readonly blacklist: string[] = [];

  public constructor(
    private readonly userDataRepo: UserDataRepository,
    private jwtService: JwtService,
  ) { }

  public async logIn(user: UserLoginDTO): Promise<{ accessToken: string }> {
    const foundUser: UserDisplayDTO =
      await this
        .userDataRepo
        .findUserDisplayDTOByEmail(
          user.email,
        );

    if (!foundUser) {
      throw new ProjectSystemError('Invalid username or password!', 409);
    }

    if (!(await this.userDataRepo.validateUserPassword(user))) {
      throw new ProjectSystemError('Invalid username or password!', 409);
    }

    if (!foundUser.hasAccess) {
      throw new ProjectSystemError(
        'Account has no access. Please contact an administrator.', 401);
    }
    
    const payload: UserDisplayDTO = { ...foundUser };
    return {
      accessToken: await this.jwtService.signAsync(payload)
    };
  }

  public blacklistToken(token: string): void {
    this.blacklist.push(token);
  }


  public isTokenBlacklisted(token: string): boolean {
    return this.blacklist.includes(token);
  }
}
