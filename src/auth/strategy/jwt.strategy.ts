import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ConfigService } from "@nestjs/config";
import { Strategy, ExtractJwt } from 'passport-jwt';
import {
  UserDataRepository
} from "../../database/repositories/user-data.repository";
import { UserDisplayDTO } from "../../models/users/user-display.dto";
import { InjectRepository } from "@nestjs/typeorm";


@Injectable()
export class JWtStrategy extends PassportStrategy(Strategy) {
  public constructor(
    @InjectRepository(UserDataRepository)
    private userDataRepo: UserDataRepository,
    configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get('JWT_SECRET'),
      ignoreExpiration: false,
    });
  }

  public async validate(payload: UserDisplayDTO): Promise<UserDisplayDTO> {
    const user: UserDisplayDTO =
      await this
        .userDataRepo
        .findUserDisplayDTOByEmail(
          payload.email
        );

    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
