import { IsString } from "class-validator";

export class SkillCreateDTO {
  @IsString()
  public skillName: string;
}