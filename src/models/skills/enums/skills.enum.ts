export enum CompanySkills {
  JAVASCRIPT = 'JAVASCRIPT',
  PYTHON = 'PYTHON',
  PHP = 'PHP',
  ANGULAR = 'ANGULAR',
  VUEJS = 'VUEJS',
  LARAVEL = 'LARAVEL',
  HTML = 'HTML',
  CSS = 'CSS',
  UIUX = 'UIUX',
  MARKETING = 'MARKETING',
  DESIGN = 'DESIGN',
  SALES = 'SALES',
  MANAGEMENT = 'MANAGEMENT'
}