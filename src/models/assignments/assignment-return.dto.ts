import { Expose } from "class-transformer";

export class AssignmentReturnDTO {
  @Expose()
  public id: string;

  @Expose()
  public assignmentSkill: string;

  @Expose()
  public employeeWorkInputPerDay: number;

  @Expose()
  public totalTimeWorked: number;

  @Expose()
  public projectId: string;

  @Expose()
  public requirementId: string;

  @Expose()
  public assigneeId: string;

  @Expose()
  public assignerId: string;

  @Expose()
  public isCompleted: boolean;

  @Expose()
  public dateAssigned: string;

  @Expose()
  public lastUpdated: string;
}
