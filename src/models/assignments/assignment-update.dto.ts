import { IsNotEmpty } from "class-validator";

export class AssignmentUpdateDTO {
  @IsNotEmpty()
  public employeeWorkInputPerDay: number;
}
