import { IsUUID, IsNotEmpty } from "class-validator";

export class AssignmentCreateDTO {
  @IsNotEmpty()
  public employeeWorkInputPerDay: number;

  @IsUUID()
  @IsNotEmpty()
  public reqId: string;
}
