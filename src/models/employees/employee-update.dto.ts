import {
  IsOptional,
  Length,
  IsString,
  MaxLength,
  IsUUID,
  IsEmail,
} from 'class-validator';

export class EmployeeUpdateDTO {
  @IsOptional()
  @IsString()
  @MaxLength(30)
  public firstName: string;

  @IsOptional()
  @IsString()
  @MaxLength(30)
  public lastName: string;

  @IsOptional()
  @IsEmail()
  @Length(2, 20)
  public email: string;

  @IsOptional()
  @IsUUID()
  public managerId: string;
}