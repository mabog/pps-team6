import {
  IsString,
  IsEmail,
  Length,
  IsNotEmpty,
  IsUUID,
  IsOptional,
} from 'class-validator';

export class EmployeeCreateDTO {
  @IsString()
  @Length(1, 30)
  public firstName: string;

  @IsString()
  @Length(1, 30)
  public lastName: string;

  @IsString()
  @IsNotEmpty()
  @IsEmail()
  public email: string;

  @IsNotEmpty()
  public totalAvailableWorkTime: number;

  @IsUUID()
  @IsOptional()
  public managerId: string;
}
