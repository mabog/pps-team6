import { Expose, Transform } from 'class-transformer';
import { Skill } from '../../database/entities/skill.entity';

export class EmployeeReturnDTO {
  @Expose()
  public id: string;

  @Expose()
  public firstName: string;

  @Expose()
  public lastName: string;

  @Expose()
  public email: string;

  @Expose()
  @Transform((_, obj) => obj.skills.map((skill: Skill) => skill.skillName))
  public skills: string[];

  @Expose()
  public totalAvailableWorkTime: number;

  @Expose()
  public dateCreated: string;

  @Expose()
  public lastUpdated: string;
}