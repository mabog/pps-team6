import {
  IsOptional,
  IsString,
  MinLength,
  Matches,
  IsBoolean
} from "class-validator";

export class EmployeePromoteDTO {
  @IsOptional()
  @IsBoolean()
  public isManager: boolean;

  @IsOptional()
  @IsString()
  @MinLength(8)
  @Matches(
    /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
    {
      message:
        'Password is too weak! \
        It must be at least 8 characters, and contain at least 1 number, lower and uppercase letter.'
    })
  public password: string;
}