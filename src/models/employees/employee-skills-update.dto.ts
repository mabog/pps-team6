import { IsOptional, IsString } from "class-validator";

export class EmployeeSkillsUpdateDTO {
  @IsOptional()
  @IsString()
  public newSkills: string;

  @IsOptional()
  @IsString()
  public skillToRemove: string;
}