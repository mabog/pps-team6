export class RequirementLongestCompletionDateDTO {
  public daysUntilCompletion: number;

  public estCompletionDate: string;
}