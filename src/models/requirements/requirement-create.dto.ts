import { IsString, IsNotEmpty } from "class-validator";

export class RequirementCreateDTO {
  @IsString()
  @IsNotEmpty()
  public reqSkill: string;

  @IsNotEmpty()
  public totalReqWorkTime: number;
}
