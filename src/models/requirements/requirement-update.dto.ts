import { IsOptional, IsString } from "class-validator";

export class RequirementUpdateDTO {
  @IsOptional()
  @IsString()
  public reqSkill: string;

  @IsOptional()
  public totalReqWorkTime: number;

  @IsOptional()
  public completedWorkTimePerDay: number;

  @IsOptional()
  public totalCompletedWorkTime: number;
}
