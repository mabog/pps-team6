import { Expose } from "class-transformer";

export class RequirementReturnDTO {
  @Expose()
  public id: string;

  @Expose()
  public reqSkill: string;

  @Expose()
  public totalReqWorkTime: number;

  @Expose()
  public leftReqWorkTime: number;

  @Expose()
  public completedWorkTimePerDay: number;

  @Expose()
  public totalCompletedWorkTime: number;

  @Expose()
  public daysUntilCompletion: number;

  @Expose()
  public estCompletionDate: string;

  @Expose()
  public isCompleted: boolean;

  @Expose()
  public dateCreated: string;

  @Expose()
  public lastUpdated: string;
}