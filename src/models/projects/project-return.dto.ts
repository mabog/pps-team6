import { Expose } from "class-transformer";

export class ProjectReturnDTO {
  @Expose()
  public id: string;

  @Expose()
  public name: string;

  @Expose()
  public description: string;

  @Expose()
  public status: string;

  @Expose()
  public projectCreator: string;

  @Expose()
  public currentManagerId: string;

  @Expose()
  public targetInDays: number;

  @Expose()
  public isWithinTarget: boolean;

  @Expose()
  public managerWorkInputPerDay: number;

  @Expose()
  public daysUntilCompletion: number;

  @Expose()
  public estCompletionDate: string;

  @Expose()
  public isCompleted: boolean;

  @Expose()
  public dateCreated: Date;

  @Expose()
  public lastUpdated: Date;
}
