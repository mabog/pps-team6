import { IsString, Length, IsNotEmpty, IsOptional } from "class-validator";

export class ProjectUpdateDTO {
  @IsString()
  @IsOptional()
  @Length(2, 80)
  public name: string;

  @IsString()
  @IsOptional()
  @Length(2, 3000)
  public description: string;

  @IsNotEmpty()
  @IsOptional()
  public targetInDays: number;

  @IsNotEmpty()
  @IsOptional()
  public managerWorkInputPerDay: number;
}