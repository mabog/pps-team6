export enum ProjectStatus {
  IN_PROGRESS = 'IN_PROGRESS',
  STOPPED = 'STOPPED',
  COMPLETED = 'COMPLETED'
}