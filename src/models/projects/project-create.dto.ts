import { IsString, Length, IsNotEmpty } from "class-validator";

export class ProjectCreateDTO {
  @IsString()
  @Length(2, 80)
  public name: string;

  @IsString()
  @Length(2, 3000)
  public description: string;

  @IsNotEmpty()
  public targetInDays: number;

  @IsNotEmpty()
  public managerWorkInputPerDay: number;
}