import {
  IsString,
  IsEmail,
  MinLength,
  Matches,
  Length,
  IsNotEmpty,
  IsUUID,
  IsOptional
} from 'class-validator';

export class UserCreateDTO {
  @IsString()
  @Length(1, 30)
  public firstName: string;

  @IsString()
  @Length(1, 30)
  public lastName: string;

  @IsString()
  @IsNotEmpty()
  @IsEmail()
  public email: string

  @IsString()
  @MinLength(8)
  @Matches(
    /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
    {
      message:
        'Password is too weak! \
        It must be at least 8 characters, and contain at least 1 number, lower and uppercase letter.'
    })
  public password: string;

  @IsNotEmpty()
  public totalAvailableWorkTime: number;

  @IsUUID()
  @IsOptional()
  public managedById: string;
}