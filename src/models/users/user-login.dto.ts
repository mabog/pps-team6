import { IsString, IsNotEmpty, IsEmail, MinLength } from 'class-validator';

export class UserLoginDTO {
  @IsString()
  @IsEmail()
  @IsNotEmpty()
  public email: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  public password: string;
}