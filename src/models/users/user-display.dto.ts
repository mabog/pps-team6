import { Expose, Transform } from "class-transformer";
import { Position } from "../../database/entities/position.entity";

export class UserDisplayDTO {
  @Expose()
  public email: string;

  @Expose()
  @Transform((_, obj) => obj.positions.map((pos: Position) => pos.positionName))
  public positions: string[];

  @Expose()
  public hasAccess: boolean;
}
