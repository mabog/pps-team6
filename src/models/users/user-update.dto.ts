import {
  IsOptional,
  Length,
  IsString,
  MaxLength,
  MinLength,
  Matches
} from 'class-validator';

export class UserUpdateDTO {
  @IsOptional()
  @IsString()
  @MaxLength(30)
  public firstName: string;

  @IsOptional()
  @IsString()
  @MaxLength(30)
  public lastName: string;

  @IsOptional()
  @IsString()
  @Length(2, 20)
  public email: string;

  @IsOptional()
  @IsString()
  @MinLength(8)
  @Matches(
    /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
    {
      message:
        'Password is too weak! \
        It must be at least 8 characters, and contain at least 1 number, lower and uppercase letter.'
    })
  public password: string;


  @IsOptional()
  public avatarUrl: string;
}
