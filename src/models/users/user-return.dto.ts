import { Expose, Transform } from 'class-transformer';
import { Position } from '../../database/entities/position.entity';
import { Skill } from '../../database/entities/skill.entity';

export class UserReturnDTO {
  @Expose()
  public id: string;

  @Expose()
  public firstName: string;

  @Expose()
  public lastName: string;

  @Expose()
  public email: string;

  @Expose()
  public totalAvailableWorkTime: number;

  @Expose()
  public isAdmin: boolean;

  @Expose()
  public isSelfManaged: boolean;

  @Expose()
  public managerId: string;

  @Expose()
  @Transform((_, obj) => obj.positions.map((pos: Position) => pos.positionName))
  public positions: string[];

  @Expose()
  @Transform((_, obj) => obj.skills.map((skill: Skill) => skill.skillName))
  public skills: string[];

  @Expose()
  public dateCreated: string;

  @Expose()
  public lastUpdated: string;

  @Expose()
  public hasAccess: boolean;

  @Expose()
  avatarUrl: string;
}