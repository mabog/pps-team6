import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { UserModule } from './resources/users/users.module';
import { EmployeeModule } from './resources/employees/employees.module';
import { ProjectModule } from './resources/projects/projects.module';
import { CoreModule } from './core/core.module';
import { DatabaseModule } from './database/database.module';
import { SkillsModule } from './resources/skills/skills.module';
import { AssignmentsModule } from './resources/assignments/assignments.module';
import {
  RequirementsModule
} from './resources/requirements/requirements.module';

@Module({
  imports: [
    UserModule,
    EmployeeModule,
    ProjectModule,
    CoreModule,
    DatabaseModule,
    SkillsModule,
    RequirementsModule,
    AssignmentsModule,
  ],
  controllers: [AppController],
})
export class AppModule { }
